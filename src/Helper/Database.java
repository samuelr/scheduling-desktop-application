/**
 * Singleton MySQL Database
 */
package Helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class Database {

	/**
	 * Holds a reference to the object
	 */
	private static Database instance
			= new Database(
					"com.mysql.jdbc.Driver",
					"U04GHm",
					"52.206.157.109",
					"mysql",
					"U04GHm",
					"53688231459"
			);

	/**
	 * Static method to create and return the database instance
	 *
	 * @return Database
	 */
	public static Database getInstance() {
		return instance;
	}

	public static String buildKeyValuePairs(HashMap<String, String> data) {
		StringBuilder kvpString = new StringBuilder();
		// iterate the hashmap
		data.keySet().forEach((key) -> {
			kvpString.append(key).append("='").append(data.get(key)).append("',");
		});
		// delete the last comma
		kvpString.delete(kvpString.length() - 1, kvpString.length());
		return kvpString.toString();
	}

	private Connection connection = null;
	private String driver;
	private String db;
	private String host;
	private String vendor;
	private String url;
	private String user;
	private String pass;

	/**
	 * Private constructor to keep this class from being instantiated
	 *
	 */
	private Database(String driver, String db, String host, String vendor, String user, String pass) {
		this.driver = driver;
		this.db = db;
		this.host = host;
		this.vendor = vendor;
		this.url = "jdbc:" + vendor + "://" + host + "/" + db;
		this.user = user;
		this.pass = pass;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Connection getConnection() {
		return connection;
	}

	public void open() {
		if (connection == null) {
			try {
				Class.forName(driver);
			} catch (ClassNotFoundException ex) {
				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "Driver not found", ex);
			}
			try {
				connection = DriverManager.getConnection(url, user, pass);
				System.out.println("Connected to database : " + db);
			} catch (SQLException ex) {
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, "Error connecting to database", ex);
			}
		}
	}

	public void close() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException ex) {
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
			}
			connection = null;
		}
	}

	public boolean isConnected() {
		boolean valid = false;
		try {
			valid = connection.isValid(15);
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		}
		return valid;
	}

	public synchronized ResultSet query(String query) {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}

		return rs;
	}

	public synchronized Integer insert(String table, HashMap<String, String> data) {
		if (!isConnected()) {
			open();
		}
		// prepare the query
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO ").append(table).append(" SET ");

		// iterate the hashmap
		data.keySet().forEach((key) -> {
			query.append(key).append("='").append(data.get(key)).append("',");
		});
		// delete the last comma and add a semicolon
		query.delete(query.length() - 1, query.length()).append(";");
		Statement stmt = null;
		Integer lastId = 0;

		try {
			// execute the query
			stmt = connection.createStatement();

			stmt.execute(query.toString(), Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			lastId = rs.getInt(1);
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		}

		return lastId;
	}

	public synchronized boolean update(String table, HashMap<String, String> data, String keyColumn) {
		Statement stmt = null;
		int rowCount = 0;
		
		//build query string
		String updateString
				= "UPDATE `" + table + "` SET " + buildKeyValuePairs(data)
				+ " WHERE " + keyColumn + "=" + data.get(keyColumn);

		// execute query string
		try {
			// execute the query
			stmt = connection.createStatement();

			rowCount = stmt.executeUpdate(updateString);
			if (rowCount == 0) {
				// there was an error
				return false;
			}
		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		// return result
		return rowCount != 0;
	}

	/**
	 *
	 * @param table
	 * @param keyColumn
	 * @param valueString
	 * @return boolean
	 */
	public synchronized boolean delete(String table, String keyColumn, String valueString) {
		Statement stmt = null;
		int rowCount = 0;

		// build query string
		String deleteString
				= "DELETE FROM " + table + " WHERE " + keyColumn + "=" + valueString;

		// execute query string
		try {
			// execute the query
			stmt = connection.createStatement();

			rowCount = stmt.executeUpdate(deleteString);

		} catch (SQLException ex) {
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		// return result
		return rowCount != 0;
	}
}
