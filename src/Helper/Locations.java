/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Samuel
 */
public class Locations {
	
	private static HashMap<String, String> locationMap = new HashMap<>();
	
	static {
		locationMap.put("PHOENIX", "Phoenix, AZ, USA");
		locationMap.put("NEWYORK", "New York, NY, USA");
		locationMap.put("LONDON", "London, England, UK");
	}
	
	/**
	 * 
	 * @return HashMap
	 */
	public static HashMap<String, String> toHashMap() {
		return new HashMap<>(locationMap);
	}
	
	/**
	 *
	 * @return ObservableList
	 */
	public static ObservableList<String> toObservable() {
		return FXCollections.observableArrayList(locationMap.values());
	}
}
