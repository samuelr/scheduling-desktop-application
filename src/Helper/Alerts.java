/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Samuel
 */
public class Alerts {

	public static void alert(String message, Alert.AlertType type, String title, String headerText) {
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(message);
		Optional<ButtonType> result = alert.showAndWait();
	}

	/**
	 *
	 * @param message
	 * @param title
	 */
	public static void info(String message, String title) {
		alert(message, Alert.AlertType.INFORMATION, title, null);
	}

	public static void info(String message) {
		alert(message, Alert.AlertType.INFORMATION, null, null);
	}
}
