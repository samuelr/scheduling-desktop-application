
//import Helper.SingletonMySQLDatabase;
//
///**
// *
// * @author Samuel
// */
//public class Database extends SingletonMySQLDatabase {
//	private Database(String driver, String db, String host, String vendor, String user, String pass) {
//		this.driver = driver;
//		this.db = db;
//		this.host = host;
//		this.vendor = vendor;
//		this.url = "jdbc:" + vendor + "://" + host + "/" + db;
//		this.user = user;
//		this.pass = pass;
//	}
//}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package Helper;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.HashMap;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author Samuel
// */
//public class Database {
//
//	private Connection connection = null;
//	private static Connection staticConnection = null;
//
//	private String driver;
//	private String db;
//	private String host;
//	private String vendor;
//	private String url;
//	private String user;
//	private String pass;
//
////	private String driver = "com.mysql.jdbc.Driver";
////	private String db = "U04GHm";
////	private String host = "52.206.157.109";
////	private String url = "jdbc:mysql://" + host + "/" + db;
////	private String user = "U04GHm";
////	private String pass = "53688231459";
//	private Database(String driver, String db, String host, String vendor, String user, String pass) {
//		this.driver = driver;
//		this.db = db;
//		this.host = host;
//		this.vendor = vendor;
//		this.url = "jdbc:" + vendor + "://" + host + "/" + db;
//		this.user = user;
//		this.pass = pass;
//	}
//
//	private Database() {
//
//	}
//
//	public static Database getDatabase() {
//		return new Database("com.mysql.jdbc.Driver", "U04GHm", "52.206.157.109", "mysql", "U04GHm", "53688231459");
//	}
//
//	public String getDriver() {
//		return driver;
//	}
//
//	public void setDriver(String driver) {
//		this.driver = driver;
//	}
//
//	public String getDb() {
//		return db;
//	}
//
//	public void setDb(String db) {
//		this.db = db;
//	}
//
//	public String getHost() {
//		return host;
//	}
//
//	public void setHost(String host) {
//		this.host = host;
//	}
//
//	public String getVendor() {
//		return vendor;
//	}
//
//	public void setVendor(String vendor) {
//		this.vendor = vendor;
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//	public String getUser() {
//		return user;
//	}
//
//	public void setUser(String user) {
//		this.user = user;
//	}
//
//	public String getPass() {
//		return pass;
//	}
//
//	public void setPass(String pass) {
//		this.pass = pass;
//	}
//
//	public Connection getConnection() {
//		return connection;
//	}
//
//	public void open() {
//		if (connection == null) {
//			try {
//				Class.forName(driver);
//			} catch (ClassNotFoundException ex) {
//				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			}
//			try {
//				connection = DriverManager.getConnection(url, user, pass);
//			} catch (SQLException ex) {
//				System.out.println("SQLException: " + ex.getMessage());
//				System.out.println("SQLState: " + ex.getSQLState());
//				System.out.println("VendorError: " + ex.getErrorCode());
//				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			}
//			System.out.println("Connected to database : " + db);
//		}
//	}
//
//	public static void staticOpen() throws ClassNotFoundException {
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			staticConnection = DriverManager.getConnection("jdbc:mysql://52.206.157.109/U04GHm", "U04GHm", "53688231459");
//			System.out.println("Statically connected to database.");
//		} catch (SQLException e) {
//			System.out.println("SQLException: " + e.getMessage());
//			System.out.println("SQLState: " + e.getSQLState());
//			System.out.println("VendorError: " + e.getErrorCode());
//		}
//	}
//
//	public static void staticClose() {
//		if (staticConnection != null) {
//			try {
//				staticConnection.close();
//			} catch (SQLException ex) {
//				System.out.println("SQLException: " + ex.getMessage());
//				System.out.println("SQLState: " + ex.getSQLState());
//				System.out.println("VendorError: " + ex.getErrorCode());
//				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			}
//		}
//	}
//
//	public static ResultSet staticQuery(String query) {
//		Statement stmt = null;
//		ResultSet rs = null;
//		try {
//			stmt = staticConnection.createStatement();
//			rs = stmt.executeQuery(query);
//		} catch (SQLException ex) {
//			System.out.println("SQLException: " + ex.getMessage());
//			System.out.println("SQLState: " + ex.getSQLState());
//			System.out.println("VendorError: " + ex.getErrorCode());
//			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			return null;
//		}
//
//		return rs;
//	}
//
//	public void close() {
//		if (connection != null) {
//			try {
//				connection.close();
//			} catch (SQLException ex) {
//				System.out.println("SQLException: " + ex.getMessage());
//				System.out.println("SQLState: " + ex.getSQLState());
//				System.out.println("VendorError: " + ex.getErrorCode());
//				Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			}
//			connection = null;
//		}
//	}
//
//	public boolean isConnected() {
//		boolean valid = false;
//		try {
//			valid = connection.isValid(15);
//		} catch (SQLException ex) {
//			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//		}
//		return valid;
//	}
//
//	public ResultSet query(String query) {
//		Statement stmt = null;
//		ResultSet rs = null;
//		try {
//			stmt = connection.createStatement();
//			rs = stmt.executeQuery(query);
//		} catch (SQLException ex) {
//			System.out.println("SQLException: " + ex.getMessage());
//			System.out.println("SQLState: " + ex.getSQLState());
//			System.out.println("VendorError: " + ex.getErrorCode());
//			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//			return null;
//		}
//
//		return rs;
//	}
//
//	public Integer insert(String table, HashMap<String, String> data) {
//		if (!isConnected()) {
//			open();
//		}
//		// prepare the query
//		StringBuilder query = new StringBuilder();
//		query.append("INSERT INTO ").append(table).append(" SET ");
//
//		// iterate the hashmap
//		data.keySet().forEach((key) -> {
//			query.append(key).append("='").append(data.get(key)).append("',");
//		});
//		// delete the last comma and add a semicolon
//		query.delete(query.length() - 1, query.length()).append(";");
//		Statement stmt = null;
//		Integer lastId = null;
//
//		try {
//			// execute the query
//			stmt = connection.createStatement();
//
//			stmt.execute(query.toString(), Statement.RETURN_GENERATED_KEYS);
//			ResultSet rs = stmt.getGeneratedKeys();
//			rs.next();
//			lastId = rs.getInt(1);
//		} catch (SQLException ex) {
//			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//		}
//
//		return lastId;
//	}
//}
