/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import Model.AddressRepository;
import Model.AppointmentRepository;
import Model.CityRepository;
import Model.CountryRepository;
import Model.CustomerRepository;
import Model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class Schedule {

	private static CustomerRepository customerRepo;
	private static AppointmentRepository appointmentRepo;
	public static LocalTime businessStart;
	public static LocalTime businessEnd;

	static {
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);
		appointmentRepo = new AppointmentRepository(Database.getInstance(), customerRepo);

		businessStart = LocalTime.of(9, 0);
		businessEnd = LocalTime.of(17, 0);
	}

	public static boolean checkPendingAppointments(User user) {
		System.out.println(String.format("Checking pending appointments for user: %s", user.getUserName()));
		return (appointmentRepo.queryByRange(ZonedDateTime.now().withSecond(0), ZonedDateTime.now().withSecond(0).plusMinutes(15)) > 0);
	}

	public static boolean isWithinHours(LocalTime start, LocalTime end) {
		boolean withinHours = false;
		// are both times after or equal to businessStart and before or equal to businessEnd?
		if (start.isAfter(businessStart)
				&& start.isBefore(businessEnd)
				&& end.isAfter(businessStart)
				&& end.isBefore(businessEnd)
				|| start.equals(businessStart)
				|| end.equals(businessEnd)) {
			withinHours = true;
		}
		return withinHours;
	}

	public static boolean overlapsOther(ZonedDateTime start, ZonedDateTime end, int currentId) {
		// an appointent overlaps if:
		// start >= other.start && start < other.end
		// OR
		// end > other.start && end < other.end

		String queryFormat = "SELECT count(*) as count FROM appointment WHERE userId=%d AND %s(('%s' >= start and '%s' < end) "
				+ "OR ('%s' > start AND '%s' <= end))";
		String query = String.format(queryFormat,
				Auth.getLoggedInUser().getId(),
				(currentId > 0) ? String.format("appointmentId <> %s AND ", currentId) : "",
				start.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")),
				start.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")),
				end.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")),
				end.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss"))
				
		);

		ResultSet rs = null;
		Database db = Database.getInstance();

		rs = db.query(query);

		int count = 0;

		try {
			if (rs.first()) {
				count = rs.getInt("count");
			}
		} catch (SQLException ex) {
			Logger.getLogger(AppointmentRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return (count > 0);
	}
}
