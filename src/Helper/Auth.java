/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import Model.User;
import Model.UserRepository;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Samuel
 */
public class Auth {

	private static User loggedInUser = null;
	private static String loggedInLocation = null;

	/**
	 * Authenticate a user
	 * 
	 * @param userName
	 * @param password
	 * @return True on success, False on failure
	 */
	public static boolean login(String userName, String password) {
		// get a user repository and search by user name
		UserRepository userRepo = new UserRepository(Database.getInstance());
		User user = userRepo.getByName(userName);

		if (user == null) {
			// user not found
			showLoginErrorAlert();
			return false;
		}

		// check password
		if (!user.getPassword().equals(Crypto.md5(password))) {
			// no match
			showLoginErrorAlert();
			return false;
		}

		// check succeeded
		// if a database needs to be updated, do it here
		
		// set logged in user
		loggedInUser = user;
		recordLogin(user);
		
		return true;
	}

	/**
	 * Gets the currently logged in user
	 *
	 * @return A User object representing the currently logged in user, or null on failure.
	 */
	public static User getLoggedInUser() {
		return loggedInUser;
	}

	public static String getLoggedInLocation() {
		return loggedInLocation;
	}
	
	/**
	 * Logs the current user out.
	 */
	public static void logout() {
		// if a database needs to be updated, do it here
		loggedInUser = null;
	}
	
	private static void showLoginErrorAlert() {
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle(I18N.get("login.error.title"));
		alert.setHeaderText(null);
		alert.setContentText(I18N.get("login.error.msg"));
		Optional<ButtonType> result = alert.showAndWait();
	}
	
	/**
	 * Records the date and time a user logs in to a text file
	 * 
	 * @param user The User object representing the user who just logged in
	 */
	private static void recordLogin(User user) {
		/*
			this will record to a text file
			for the purposes of this project, I will create a temporary
			file and delete it on program exit
			*/
		try {
			// create temp file
			File logfile = File.createTempFile("evaluation-temp-", "-delete-me");
			logfile.deleteOnExit();
			// creat the logger
			
			Logger logger = Logger.getLogger("UserLog");
			
			// create a formatter
			SimpleFormatter formatter = new SimpleFormatter();
			// create the file handler
			FileHandler fh = new FileHandler(logfile.getPath());
			// give the formatter to the file handler
			fh.setFormatter(formatter);
			
			// give the file handler to the logger
			logger.addHandler(fh);
			
			logger.info("Logging user login to file: " + logfile.getPath());
			logger.info("User logged in: " + user.getUserName() + " (" + user.getId() + ").");
			
		} catch (IOException ex) {
			Logger.getLogger(Auth.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
