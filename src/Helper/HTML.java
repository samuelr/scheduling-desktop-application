/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import javax.swing.text.DateFormatter;

/**
 *
 * @author Samuel
 */
public class HTML {
	private static int counter = 0;
	/**
	 *
	 * @param data
	 * @return
	 */
	public static String getShcheduleMarkup(HashMap<String, ArrayList<HashMap<String, String>>> data) {
		StringBuilder content = new StringBuilder();
		
		// Lambda expression
		// iterates over the data to render an HTML <ul>
		// uses lambda instead of
		// data.forEach(new BiConsumer<String, ArrayList<HashMap>>() { @Override	public void accept()...
		content.append("<div id=\"userSchedule\" class=\"section group\">");
		data.forEach((String user, ArrayList<HashMap<String, String>> appointmentData) -> {
			content.append(String.format("<div class=\"col span_2_of_2\"><span class=\"userName label\">%s</span></div>", user));
			
			content.append("<div class=\"appointment section group\">");
			counter = 0;
			appointmentData.forEach((HashMap item) -> {
				if ((counter > 0) && ((counter % 2) == 0)) {
					// close out the last row and start a new one
					content.append("</div><div class=\"appointment section group\">");
				}
				content.append("<div class=\"col span_1_of_2\"><p>");
				content.append(String.format("<span class=\"label\">Customer</span>%s<br>", item.get("customerName")));
				content.append(String.format("<span class=\"label\">Title</span>%s<br>", item.get("title")));
				content.append(String.format("<span class=\"label\">Type</span>%s<br>", item.get("type")));
				content.append(String.format("<span class=\"label\">Description</span>%s<br>", item.get("description")));
				content.append(String.format("<span class=\"label\">Start</span>%s<br>", item.get("start")));
				content.append(String.format("<span class=\"label\">End</span>%s<br>", item.get("end")));
				content.append(String.format("<span class=\"label\">Contact</span>%s<br>", item.get("contact")));
				content.append(String.format("<span class=\"label\">Url</span>%s<br>", item.get("url")));
				content.append(String.format("<span class=\"label\">Location</span>%s<br>", item.get("location")));
				content.append("</p></div>");
				counter += 1;
			});
			content.append("</div>");
		});
		content.append("</div>");

		return content.toString();
	}

	public static CharSequence getByCustomerMarkup(HashMap<String, ArrayList<HashMap<String, String>>> data) {
		StringBuilder content = new StringBuilder();
		
		// Lambda expression
		// iterates over the data to render an HTML <ul>
		// uses lambda instead of
		// data.forEach(new BiConsumer<String, ArrayList<HashMap>>() { @Override	public void accept()...
		content.append("<div id=\"userSchedule\" class=\"section group\">");
		data.forEach((String user, ArrayList<HashMap<String, String>> appointmentData) -> {
			content.append(String.format("<div class=\"col span_2_of_2\"><span class=\"customerName label\">%s</span></div>", user));
			
			content.append("<div class=\"appointment section group\">");
			counter = 0;
			appointmentData.forEach((HashMap item) -> {
				if ((counter > 0) && ((counter % 2) == 0)) {
					// close out the last row and start a new one
					content.append("</div><div class=\"appointment section group\">");
				}
				content.append("<div class=\"col span_1_of_2\"><p>");
				content.append(String.format("<span class=\"label\">Consultant</span>%s<br>", item.get("userName")));
				content.append(String.format("<span class=\"label\">Title</span>%s<br>", item.get("title")));
				content.append(String.format("<span class=\"label\">Type</span>%s<br>", item.get("type")));
				content.append(String.format("<span class=\"label\">Description</span>%s<br>", item.get("description")));
				content.append(String.format("<span class=\"label\">Start</span>%s<br>", item.get("start")));
				content.append(String.format("<span class=\"label\">End</span>%s<br>", item.get("end")));
				content.append(String.format("<span class=\"label\">Contact</span>%s<br>", item.get("contact")));
				content.append(String.format("<span class=\"label\">Url</span>%s<br>", item.get("url")));
				content.append(String.format("<span class=\"label\">Location</span>%s<br>", item.get("location")));
				content.append("</p></div>");
				counter += 1;
			});
			content.append("</div>");
		});
		content.append("</div>");

		return content.toString();
	}

	public static CharSequence getAppointmentTypesMarkup(HashMap<Integer, ArrayList<HashMap<String, String>>> reportData) {
		StringBuilder content = new StringBuilder();
		
		// Lambda expression
		// iterates over the data to render an HTML <ul>
		// uses lambda instead of
		// data.forEach(new BiConsumer<String, ArrayList<HashMap>>() { @Override	public void accept()...
		content.append("<div id=\"userSchedule\" class=\"section group\">");
		reportData.forEach((Integer month, ArrayList<HashMap<String, String>> appointmentData) -> {
			content.append(String.format("<div class=\"col span_2_of_2\"><span class=\"month label\">%s</span></div>", Month.of(month).getDisplayName(TextStyle.FULL, I18N.getLocale())));
			
			content.append("<div class=\"appointment section group\">");
			content.append("<div class=\"col span_2_of_2\"><p>");
			appointmentData.forEach((HashMap item) -> {
				
				content.append(String.format("<span class=\"label\">%s</span>%s<br>", item.get("Type"),item.get("Count")));
			});
			content.append("</p></div>");
			content.append("</div>");
		});
		content.append("</div>");

		return content.toString();
	}
}
/*

<div class="section group">
	<div class="col span_2_of_2">test</div>
</div>
<div class="section group">
	<div class="col span_1_of_2">
		<p><strong>Customer</strong>{customerName}<br>
		</p>
	</div>
	<div class="col span_1_of_2">
		<p><strong>Customer</strong>{customerName}<br>
		</p>
	</div>
</div>

<ul class="userSchedule>
			<li>test
				<ul section>
					<li><span class="label">Customer</span>{customerName}</li>
					<li><span class="label">Title</span>{title}</li>
					<li><span class="label">Type</span>{type}</li>
					<li><span class="label">Description</span>{description}</li>
					<li><span class="label">Start</span>{start}</li>
					<li><span class="label">End</span>{end}</li>
					<li><span class="label">Contact</span>{contact}</li>
					<li><span class="label">Url</span>{url}</li>
					<li><span class="label">Location</span>{location}</li>
				</ul>
			</li>
			<li>jeffgordon
				<ul>
					<li>
						<ul>
							<li><span class="label">Customer</span>{customerName}</li>
							<li><span class="label">Title</span>{title}</li>
							<li><span class="label">Type</span>{type}</li>
							<li><span class="label">Description</span>{description}</li>
							<li><span class="label">Start</span>{start}</li>
							<li><span class="label">End</span>{end}</li>
							<li><span class="label">Contact</span>{contact}</li>
							<li><span class="label">Url</span>{url}</li>
							<li><span class="label">Location</span>{location}</li>
						</ul>
					</li>
					<li>
						<ul>
							<li><span class="label">Customer</span>{customerName}</li>
							<li><span class="label">Title</span>{title}</li>
							<li><span class="label">Type</span>{type}</li>
							<li><span class="label">Description</span>{description}</li>
							<li><span class="label">Start</span>{start}</li>
							<li><span class="label">End</span>{end}</li>
							<li><span class="label">Contact</span>{contact}</li>
							<li><span class="label">Url</span>{url}</li>
							<li><span class="label">Location</span>{location}</li>
						</ul>
					</li>
					<li>
						<ul>
							<li><span class="label">Customer</span>{customerName}</li>
							<li><span class="label">Title</span>{title}</li>
							<li><span class="label">Type</span>{type}</li>
							<li><span class="label">Description</span>{description}</li>
							<li><span class="label">Start</span>{start}</li>
							<li><span class="label">End</span>{end}</li>
							<li><span class="label">Contact</span>{contact}</li>
							<li><span class="label">Url</span>{url}</li>
							<li><span class="label">Location</span>{location}</li>
						</ul>
					</li>
				</ul>
			</li>
			<li>sallystruthers
				<ul>
					<li><span class="label">Customer</span>{customerName}</li>
					<li><span class="label">Title</span>{title}</li>
					<li><span class="label">Type</span>{type}</li>
					<li><span class="label">Description</span>{description}</li>
					<li><span class="label">Start</span>{start}</li>
					<li><span class="label">End</span>{end}</li>
					<li><span class="label">Contact</span>{contact}</li>
					<li><span class="label">Url</span>{url}</li>
					<li><span class="label">Location</span>{location}</li>
				</ul>
			</li>
		</ul>
 */
