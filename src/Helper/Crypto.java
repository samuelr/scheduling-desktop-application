/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class Crypto {
	
	public static String md5(String value) {
		String md5String = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(value.getBytes(), 0, value.length());
			md5String = String.format("%032x", new BigInteger(1, digest.digest()));
		} catch (NoSuchAlgorithmException ex) {
			Logger.getLogger(Crypto.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		return md5String;
	}
	
}
