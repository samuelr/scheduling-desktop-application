/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helper;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;

/**
 *
 * @author Samuel
 */
public class AppointmentTypes {
	public static String ONBOARDING = "Onboarding";
	public static String CONSULT = "Consult";
	public static String BRAINSTORM = "Brainstorm";
	
	public static ChoiceBox<String> getDropDown() {
		ChoiceBox<String> cbReturn = new ChoiceBox<>();
		List<String> items = new ArrayList<>();
		items.add(ONBOARDING);
		items.add(CONSULT);
		items.add(BRAINSTORM);
		
		cbReturn.setItems(FXCollections.observableArrayList(items));
		return cbReturn;
	}

	public static ObservableList<String> toObservable() {
		List<String> items = new ArrayList<>();
		items.add(ONBOARDING);
		items.add(CONSULT);
		items.add(BRAINSTORM);
		
		return FXCollections.observableArrayList(items);
	}
}
