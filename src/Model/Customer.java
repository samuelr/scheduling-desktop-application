/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.ZonedDateTime;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class Customer {

	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final StringProperty name = new SimpleStringProperty();
	private final ObjectProperty<Address> address = new SimpleObjectProperty<>();
	private final BooleanProperty active = new SimpleBooleanProperty();
	private final ReadOnlyObjectWrapper createdDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	public Customer() {
	}

	public Customer(Integer id, String name, Address address, Boolean active, ZonedDateTime createdDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.name.set(name);
		this.address.set(address);
		this.active.set(active);
		this.createdDate.set(createdDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
	}

	public int getId() {
		return id.get();
	}

	public ReadOnlyIntegerProperty idProperty() {
		return id;
	}

	public String getName() {
		return name.get();
	}

	public void setName(String value) {
		name.set(value);
		isModified.set(true);
	}

	public StringProperty nameProperty() {
		return name;
	}

	public Address getAddress() {
		return address.get();

	}

	public void setAddress(Address value) {
		address.set(value);
		isModified.set(true);
	}

	public ObjectProperty addressProperty() {
		return address;
	}

	public boolean isActive() {
		return active.get();
	}

	public void setActive(boolean value) {
		active.set(value);
		isModified.set(true);
	}

	public BooleanProperty activeProperty() {
		return active;
	}

	public ZonedDateTime getCreatedDate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty createdDateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return createdBy.getReadOnlyProperty();
	}
	
	public boolean isModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}
}
