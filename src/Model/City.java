/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.ZonedDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class City {

	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final StringProperty city = new SimpleStringProperty();
	private final ObjectProperty<Country> country = new SimpleObjectProperty<>();
	private final ReadOnlyObjectWrapper createdDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	public City() {
	}

	public City(Integer id, String city, Country country, ZonedDateTime createdDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.city.set(city);
		this.country.set(country);
		this.createdDate.set(createdDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
	}

	public int getId() {
		return id.get();
	}

	protected void setId(int value) {
		this.id.set(value);
	}

	public ReadOnlyIntegerProperty idProperty() {
		return id;
	}

	public String getCity() {
		return city.get();
	}

	public void setCity(String value) {
		city.set(value);
		isModified.set(true);
	}

	public StringProperty cityProperty() {
		return city;
	}

	public Country getCountry() {
		return country.get();
	}

	public void setCountry(Country value) {
		country.set(value);
		isModified.set(true);
	}

	public ObjectProperty countryProperty() {
		return country;
	}

	public ZonedDateTime getCreatedDate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty createdDateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public boolean isModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}
}
