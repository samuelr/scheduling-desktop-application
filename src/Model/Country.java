/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.ZonedDateTime;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class Country {

	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final StringProperty country = new SimpleStringProperty();
	private final ReadOnlyObjectWrapper createdDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	/**
	 * Empty constructor to create an empty instance.
	 * Used for adding records
	 */
	public Country() {}
	
	/**
	 * Constructor with parameters for use when retrieving from the database
	 * 
	 * @param id id
	 * @param country country
	 * @param createdDate createdDate
	 * @param createdBy createdBy
	 * @param lastUpdate lastUpdate
	 * @param lastUpdateBy lastUpdateBy
	 */
	public Country(Integer id, String country, ZonedDateTime createdDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.country.set(country);
		this.createdDate.set(createdDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
		this.isModified.set(false);
	}

	public Integer getId() {
		return id.get();
	}

	protected void setId(int value) {
		this.id.set(value);
	}
	
	public ReadOnlyIntegerProperty idProperty() {
		return id.getReadOnlyProperty();
	}

	public String getCountry() {
		return country.get();
	}

	public void setCountry(String value) {
		country.set(value);
		isModified.set(true);
	}

	public StringProperty countryProperty() {
		return country;
	}
	
	public ZonedDateTime getCreatedDate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty createdDateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public boolean isModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}
	
}
