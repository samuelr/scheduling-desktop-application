/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class AddressRepository {

	private static Address createAddress(int id, String address, String address2, City city, String postalCode, String phone, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String LastUpdateBy) {

		return new Address(id, address, address2, city, postalCode, phone, createDate, createdBy, lastUpdate, LastUpdateBy);
	}

	private static Address mapToObject(ResultSet rs) throws SQLException {
		CityRepository cityRepo = new CityRepository(Database.getInstance());

		return createAddress(
				rs.getInt("addressId"),
				rs.getString("address"),
				rs.getString("address2"),
				cityRepo.get(rs.getInt("cityId")),
				rs.getString("postalCode"),
				rs.getString("phone"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("lastUpdateBy")
		);
	}

	private Database db = null;

	public AddressRepository(Database database) {
		this.db = database;
	}

	public HashMap<Integer, Address> getAll() {
		HashMap<Integer, Address> addressMap = new HashMap<>();

		//db.open();
		ResultSet rs = null;
		rs = db.query("SELECT * FROM address");

		try {
			while (rs.next()) {
				// get the data to map
				addressMap.put(rs.getInt("addressId"), mapToObject(rs));
			}
			//db.close();
		} catch (SQLException ex) {
			Logger.getLogger(AddressRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return addressMap;
	}

	public Boolean save(Address country) {
		return false;
	}

	public void create(Address item) {
		create(Collections.singletonList(item));
	}

	public void create(List<Address> items) {
		// insert a new Address into the database
		HashMap<String, String> data = new HashMap<>();
		int insertId;

		for (Address item : items) {
			try {
				// to create a record, we need address, address2, cityId, postalCode, phone, and the user who created it
				// TODO: use User object, instead of hardcoding "test"
				data.put("address", item.getAddress());
				data.put("address2", item.getAddress2());
				data.put("cityId", Integer.toString(item.getCity().getId()));
				data.put("postalCode", item.getPostalCode());
				data.put("phone", item.getPhone());
				data.put("createdBy", Auth.getLoggedInUser().getUserName());
				data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
				//db.open();
				insertId = db.insert("address", data);
				if (insertId > 0) {
					// create succeeded
					item.setId(insertId);
				}
			} catch (RuntimeException ex) {
				Logger.getLogger(AddressRepository.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				//db.close();
			}
		}
	}

	/**
	 * Save changes to an existing record in the database
	 *
	 * @param item
	 * @return boolean
	 */
	public boolean update(Address item) {
		Boolean result = false;
		HashMap<String, String> data = new HashMap<>();

		try {
			// to update a record, we need the city name, countryId, and the user who created it
			// TODO: use User object, instead of hardcoding "test"
			data.put("addressId", Integer.toString(item.getId()));
			data.put("address", item.getAddress());
			data.put("address2", item.getAddress2());
			data.put("cityId", Integer.toString(item.getCity().getId()));
			data.put("postalCode", item.getPostalCode());
			data.put("phone", item.getPhone());
			data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
			//db.open();
			result = db.update("address", data, "addressId");
			if (result == false) {
				// there was an error in updating
				Logger.getLogger(AddressRepository.class.getName()).log(Level.SEVERE, "MySql returned 0 updated rows!");
			}
		} catch (RuntimeException ex) {
			Logger.getLogger(AddressRepository.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			//db.close();
		}

		return result;
	}

	public boolean remove(Address item) {
		//db.open();
		boolean result = db.delete("address", "addressId", Integer.toString(item.getId()));
		//db.close();

		return result;
	}

	public List<Address> query(String queryString) {
		return null;
	}

	/**
	 *
	 * @param id
	 * @return Address
	 */
	public Address get(int id) {
		Address address = null;

		//db.open();
		ResultSet rs = null;
		rs = db.query("SELECT * FROM address WHERE addressId=" + Integer.toString(id));

		try {
			if (rs.next()) {
				// get the data to map
				address = mapToObject(rs);
			}
		} catch (SQLException ex) {
			Logger.getLogger(AddressRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		//db.close();		
		return address;
	}

}
