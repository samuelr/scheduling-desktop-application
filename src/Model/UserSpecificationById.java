/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Interface.SqlSpecification;
import Interface.UserSpecification;

/**
 *
 * @author Samuel
 */
public class UserSpecificationById implements UserSpecification, SqlSpecification {

	private int id;
	
	public UserSpecificationById(int id) {
		super();
		this.id = id;
	}

	@Override
	public Boolean isSatisfiedBy(User user) {
		return (user.getId() > 0);
	}

	@Override
	public String toSqlClause() {
		return String.format("WHERE userId=%s", this.id);
	}
	
}
