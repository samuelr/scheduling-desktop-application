/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.omg.CosNaming.NamingContextExtPackage.URLStringHelper;

/**
 *
 * @author Samuel
 */
public class AppointmentRepository {

	private Database db;
	private CustomerRepository customerRepo;

	public AppointmentRepository(Database database, CustomerRepository customerRepo) {
		this.db = database;
		this.customerRepo = customerRepo;
	}

	public boolean create(HashMap itemData) {
		int insertId;

		insertId = db.insert("appointment", itemData);
		if (insertId > 0) {
			// create succeeded
			return true;
		}
		return false;
	}

	public boolean update(Appointment item) {
		HashMap<String, String> itemData = new HashMap<>();

		// put the data into the hashmap
		itemData.put("appointmentId", Integer.toString(item.getId()));
		itemData.put("customerId", Integer.toString(item.getCustomer().getId()));
		itemData.put("userId", Integer.toString(item.getUser().getId()));
		itemData.put("title", item.getTitle());
		itemData.put("description", item.getDescription());
		itemData.put("location", item.getLocation());
		itemData.put("contact", item.getContact());
		itemData.put("type", item.getType());
		itemData.put("url", (item.getUrl() == null) ? "" : item.getUrl().toString());
		itemData.put("start", item.getStart().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("GMT"))));
		itemData.put("end", item.getEnd().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("GMT"))));
		itemData.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());

		return db.update("appointment", itemData, "appointmentId");
	}

	public boolean remove(Appointment item) {
		return db.delete("appointment", "appointmentId", Integer.toString(item.getId()));
	}

	public List<Appointment> getAll() {
		ArrayList<Appointment> appointments = new ArrayList();
		User user = Auth.getLoggedInUser();
		

		ResultSet rs = null;
		rs = db.query(String.format("SELECT * FROM appointment WHERE userId = %d ORDER BY start ASC", user.getId()));

		try {
			while (rs.next()) {
				// get the data to map
				Appointment appt = mapToObject(rs);
				appointments.add(appt);
			}
		} catch (SQLException ex) {
			Logger.getLogger(AppointmentRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return appointments;
	}

	private Appointment mapToObject(ResultSet rs) throws SQLException {

		// get the customer record.
		System.out.println(getClass() + ": Attempting to load customer " + rs.getInt("customerId"));
		Customer customer = customerRepo.get(rs.getInt("customerId"));

		// create the URL object
		System.out.println(getClass() + ": Attempting to create URL: " + rs.getString("url"));
		URL url = null;
		String urlString = rs.getString("url");
		try {
			if (!urlString.trim().isEmpty()) {
				url = new URL(urlString);
			}
		} catch (MalformedURLException ex) {
			Logger.getLogger(AppointmentRepository.class.getName()).log(Level.INFO, "Failed to create URL from string \"" + urlString + "\"");
			url = null;
		}

		// get the user info
		System.out.println(getClass() + ": Attempting to load user " + rs.getInt("userId"));
		UserRepository ur = new UserRepository(Database.getInstance());
		User user = ur.get(rs.getInt("userId"));

		return new Appointment(
				rs.getInt("appointmentId"),
				customer,
				user,
				rs.getString("title"),
				rs.getString("description"),
				rs.getString("location"),
				rs.getString("contact"),
				rs.getString("type"),
				url,
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("start"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("end"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("LastUpdateBy")
		);
	}

	/**
	 * Returns the number of appointments within start and end time
	 *
	 * @param start
	 * @param end
	 * @return
	 */
	public int queryByRange(ZonedDateTime start, ZonedDateTime end) {

		// build query
		String query = String.format("SELECT COUNT(*) as count FROM appointment WHERE userId=%d AND start > '%s' AND start < '%s'",
				Auth.getLoggedInUser().getId(),
				start.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
				end.withZoneSameInstant(ZoneId.of("GMT")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
		);

		ResultSet rs = null;
		rs = db.query(query);

		int count = 0;

		try {
			if (rs.first()) {
				count = rs.getInt("count");
			}
		} catch (SQLException ex) {
			Logger.getLogger(AppointmentRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return count;
	}
}
