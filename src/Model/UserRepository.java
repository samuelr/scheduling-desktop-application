/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class UserRepository {

	private static User mapToObject(ResultSet rs) throws SQLException {
		User user;

		user = new User(
				rs.getInt("userId"),
				rs.getString("userName"),
				rs.getString("password"),
				rs.getBoolean("active"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("lastUpdateBy")
		);
		return user;
	}

	private Database db;

	public UserRepository(Database db) {
		this.db = db;
	}

	public User get(int id) {

		User user = null;
		Statement stmt = null;
		ResultSet rs = null;
		//db.open();
		rs = db.query(String.format("SELECT * FROM user WHERE userId=%d", id));
		try {
			if (rs.next()) {
				user = new User(
						rs.getInt("userId"),
						rs.getString("userName"),
						rs.getString("password"),
						rs.getBoolean("active"),
						ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
						rs.getString("createdBy"),
						ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
						rs.getString("lastUpdateBy")
				);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
		}
		//db.close();
		return user;
	}

	public User getByName(String name) {

		User user = null;
		Statement stmt = null;
		ResultSet rs = null;
		//db.open();
		rs = db.query(String.format("SELECT * FROM user WHERE userName LIKE \"%s\"", name));
		try {
			if (rs.next()) {
				user = new User(
						rs.getInt("userId"),
						rs.getString("userName"),
						rs.getString("password"),
						rs.getBoolean("active"),
						ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
						rs.getString("createdBy"),
						ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
						rs.getString("lastUpdateBy")
				);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
		}
		//db.close();
		return user;
	}

	public void create(User item) {
		create(Collections.singletonList(item));
	}

	public void create(List<User> items) {
		// insert a new Country into the database
		HashMap<String, String> data = new HashMap<>();
		int insertId;

		for (User item : items) {
			try {
				// to create a record, we need only the country name and the user who created it
				// TODO: use User object, instead of hardcoding "test"
				data.put("username", item.getUserName());
				data.put("password", item.getPassword());
				data.put("active", (item.isActive()) ? "1" : "0");
				data.put("createdBy", Auth.getLoggedInUser().getUserName());
				data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
				//db.open();
				insertId = db.insert("user", data);
				if (insertId > 0) {
					// create succeeded
					item.setId(insertId);
				}
			} catch (RuntimeException ex) {
				Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				//db.close();
			}
		}
	}

	/**
	 * Save changes to an existing record in the database
	 *
	 * @param item
	 * @return boolean
	 */
	public boolean update(User item) {
		boolean result = false;
		HashMap<String, String> data = new HashMap<>();

		try {
			// to update a record, we need username, password, and active
			// TODO: use User object, instead of hardcoding "test"
			data.put("userId", Integer.toString(item.getId()));
			data.put("username", item.getUserName());
			data.put("password", item.getPassword());
			data.put("active", (item.isActive()) ? "1" : "0");
			data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
			result = db.update("user", data, "userId");
			if (result == false) {
				// there was an error in updating
				Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, "MySql returned 0 updated rows!");
			}
		} catch (RuntimeException ex) {
			Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return result;
	}

	public boolean remove(User item) {
		boolean result = db.delete("user", "userId", Integer.toString(item.getId()));
		return result;
	}

	/**
	 *
	 * @param id
	 * @return Country
	 */
	public User getById(int id) {
		User user = null;

		ResultSet rs = null;
		rs = db.query("SELECT * FROM user WHERE userId=" + Integer.toString(id));

		try {
			if (rs.next()) {
				// get the data to map
				user = mapToObject(rs);
			}
		} catch (SQLException ex) {
			Logger.getLogger(UserRepository.class.getName()).log(Level.SEVERE, null, ex);
		}
		return user;
	}
}
