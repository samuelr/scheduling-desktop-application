/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.net.URL;
import java.time.ZonedDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class Appointment {
	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final ObjectProperty<Customer> customer = new SimpleObjectProperty<>();
	private final ObjectProperty<User> user = new SimpleObjectProperty<>();
	private final StringProperty title = new SimpleStringProperty();
	private final StringProperty description = new SimpleStringProperty();
	private final StringProperty location = new SimpleStringProperty();
	private final StringProperty contact = new SimpleStringProperty();
	private final StringProperty type = new SimpleStringProperty();
	private final ObjectProperty<URL> url = new SimpleObjectProperty();
	private final ObjectProperty<ZonedDateTime> start = new SimpleObjectProperty<>();
	private final ObjectProperty<ZonedDateTime> end = new SimpleObjectProperty<>();
	private final ReadOnlyObjectWrapper<ZonedDateTime> createDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper<ZonedDateTime> lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	public Appointment() {
	}

	public Appointment(int id) {
	}

	Appointment(int id, Customer customer, User user, String title, String description, String location, String contact, String type, URL url, ZonedDateTime start, ZonedDateTime end, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.customer.set(customer);
		this.user.set(user);
		this.title.set(title);
		this.description.set(description);
		this.location.set(location);
		this.contact.set(contact);
		this.type.set(type);
		this.url.set(url);
		this.start.set(start);
		this.end.set(end);
		this.createDate.set(createDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
	}

	public int getId() {
		return id.get();
	}

	public ReadOnlyIntegerProperty idProperty() {
		return id.getReadOnlyProperty();
	}

	public Customer getCustomer() {
		return customer.get();
	}

	public void setCustomer(Customer value) {
		customer.set(value);
	}

	public ObjectProperty customerProperty() {
		return customer;
	}

	public User getUser() {
		return user.get();
	}

	public void setUser(User value) {
		user.set(value);
	}

	public ObjectProperty userProperty() {
		return user;
	}

	public String getTitle() {
		return title.get();
	}

	public void setTitle(String value) {
		title.set(value);
	}

	public StringProperty titleProperty() {
		return title;
	}

	public String getDescription() {
		return description.get();
	}

	public void setDescription(String value) {
		description.set(value);
	}

	public StringProperty descriptionProperty() {
		return description;
	}

	public String getLocation() {
		return location.get();
	}

	public void setLocation(String value) {
		location.set(value);
	}

	public StringProperty locationProperty() {
		return location;
	}

	public String getContact() {
		return contact.get();
	}

	public void setContact(String value) {
		contact.set(value);
	}

	public StringProperty contactProperty() {
		return contact;
	}

	public String getType() {
		return type.get();
	}

	public void setType(String value) {
		type.set(value);
	}

	public StringProperty typeProperty() {
		return type;
	}

	public URL getUrl() {
		return url.get();
	}

	public void setUrl(URL value) {
		url.set(value);
	}

	public ObjectProperty<URL> urlProperty() {
		return url;
	}

	public ZonedDateTime getStart() {
		return start.get();
	}

	public void setStart(ZonedDateTime value) {
		start.set(value);
	}

	public ObjectProperty startProperty() {
		return start;
	}

	public ZonedDateTime getEnd() {
		return end.get();
	}

	public void setEnd(ZonedDateTime value) {
		end.set(value);
	}

	public ObjectProperty endProperty() {
		return end;
	}


	public ZonedDateTime getCreateDate() {
		return createDate.get();
	}

	public ReadOnlyObjectProperty createDateProperty() {
		return createDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return lastUpdate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return lastUpdate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return lastUpdateBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return lastUpdateBy.getReadOnlyProperty();
	}

	public boolean isIsModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}

}
