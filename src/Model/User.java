/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Crypto;
import java.time.ZonedDateTime;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class User {

	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final StringProperty userName = new SimpleStringProperty();
	private final StringProperty password = new SimpleStringProperty();
	private final BooleanProperty active = new SimpleBooleanProperty();
	private final ReadOnlyObjectWrapper<ZonedDateTime> createDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper<ZonedDateTime> lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	public User(String userName, String password, Boolean active) {
		this.userName.set(userName);
		this.password.set(password);
		this.active.set(active);
	}

	public User(int id, String userName, String password, Boolean active, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.userName.set(userName);
		this.password.set(password);
		this.active.set(active);
		this.createDate.set(createDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
	}

	public User() {
	}

	public int getId() {
		return id.get();
	}

	protected void setId(int value) {
		this.id.set(value);
	}
	
	public ReadOnlyIntegerProperty idProperty() {
		return id.getReadOnlyProperty();
	}

	public String getUserName() {
		return userName.get();
	}

	public void setUserName(String value) {
		userName.set(value);
		isModified.set(true);
	}

	public StringProperty userNameProperty() {
		return userName;
	}

	public String getPassword() {
		return password.get();
	}

	public void setPassword(String value) {
		this.password.set(Crypto.md5(value));
		isModified.set(true);
	}

	public StringProperty passwordProperty() {
		return password;
	}

	public boolean isActive() {
		return active.get();
	}

	public void setActive(boolean value) {
		active.set(value);
		isModified.set(true);
	}

	public BooleanProperty activeProperty() {
		return active;
	}

	public ZonedDateTime getCreateDate() {
		return createDate.get();
	}

	public ReadOnlyObjectProperty createDateProperty() {
		return createDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return lastUpdate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return lastUpdate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return lastUpdateBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return lastUpdateBy.getReadOnlyProperty();
	}

	public boolean isIsModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}
}
