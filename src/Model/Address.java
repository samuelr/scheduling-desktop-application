/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.ZonedDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Samuel
 */
public class Address {

	private final ReadOnlyIntegerWrapper id = new ReadOnlyIntegerWrapper();
	private final StringProperty address = new SimpleStringProperty();
	private final StringProperty address2 = new SimpleStringProperty();
	private final ObjectProperty<City> city = new SimpleObjectProperty<>();
	private final StringProperty postalCode = new SimpleStringProperty();
	private final StringProperty phone = new SimpleStringProperty();
	private final ReadOnlyObjectWrapper createdDate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper createdBy = new ReadOnlyStringWrapper();
	private final ReadOnlyObjectWrapper lastUpdate = new ReadOnlyObjectWrapper<>();
	private final ReadOnlyStringWrapper lastUpdateBy = new ReadOnlyStringWrapper();
	private final ReadOnlyStringWrapper fullAddress = new ReadOnlyStringWrapper();
	private final ReadOnlyBooleanWrapper isModified = new ReadOnlyBooleanWrapper();

	public Address() {
	}

	public Address(Integer id, String address, String address2, City city, String postalCode, String phone, ZonedDateTime createdDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		this.id.set(id);
		this.address.set(address);
		this.address2.set(address2);
		this.city.set(city);
		this.postalCode.set(postalCode);
		this.phone.set(phone);
		this.createdDate.set(createdDate);
		this.createdBy.set(createdBy);
		this.lastUpdate.set(lastUpdate);
		this.lastUpdateBy.set(lastUpdateBy);
	}

	public int getId() {
		return id.get();
	}

	protected void setId(int value) {
		this.id.set(value);
	}
	
	public ReadOnlyIntegerProperty idProperty() {
		return id;
	}

	public String getAddress() {
		return address.get();
	}

	public void setAddress(String value) {
		address.set(value);
		isModified.set(true);
	}

	public StringProperty addressProperty() {
		return address;
	}

	public String getAddress2() {
		return address2.get();
	}

	public void setAddress2(String value) {
		address2.set(value);
		isModified.set(true);
	}

	public StringProperty address2Property() {
		return address2;
	}

	public City getCity() {
		return city.get();
	}

	public void setCity(City value) {
		city.set(value);
		isModified.set(true);
	}

	public ObjectProperty cityProperty() {
		return city;
	}

	public String getPostalCode() {
		return postalCode.get();
	}

	public void setPostalCode(String value) {
		postalCode.set(value);
		isModified.set(true);
	}

	public StringProperty postalCodeProperty() {
		return postalCode;
	}

	public String getPhone() {
		return phone.get();
	}

	public void setPhone(String value) {
		phone.set(value);
		isModified.set(true);
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public String getFullAddress() {
		String fAddress;

		// calculate address
		fAddress = this.address.get() + ", " + this.address2.get() + ", " + this.getCity().getCity() + ", " + this.getCity().getCountry().getCountry();
		fullAddress.set(fAddress);
		return fAddress;
	}

	public ReadOnlyStringProperty fullAddressProperty() {
		getFullAddress();
		return fullAddress.getReadOnlyProperty();
	}

	public ZonedDateTime getCreatedDate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty createdDateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getCreatedBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty createdByProperty() {
		return createdBy.getReadOnlyProperty();
	}

	public ZonedDateTime getLastUpdate() {
		return (ZonedDateTime) createdDate.get();
	}

	public ReadOnlyObjectProperty lastUpdateProperty() {
		return createdDate.getReadOnlyProperty();
	}

	public String getLastUpdateBy() {
		return createdBy.get();
	}

	public ReadOnlyStringProperty lastUpdateByProperty() {
		return createdBy.getReadOnlyProperty();
	}
	
	public boolean isModified() {
		return isModified.get();
	}

	public ReadOnlyBooleanProperty isModifiedProperty() {
		return isModified.getReadOnlyProperty();
	}
}
