/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class CountryRepository {

	private static Country createCountry(int id, String country, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String LastUpdateBy) {

		return new Country(id, country, createDate, createdBy, lastUpdate, LastUpdateBy);
	}

	private static Country mapToObject(ResultSet rs) throws SQLException {
		return createCountry(
				rs.getInt("countryId"),
				rs.getString("country"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("lastUpdateBy")
		);
	}

	private Database db = null;

	public CountryRepository(Database database) {
		this.db = database;
	}

	public HashMap<String, Country> getAll() {
		HashMap<String, Country> countryMap = new HashMap<>();

		//db.open();

		ResultSet rs = null;
		rs = db.query("SELECT * FROM country");

		try {
			while (rs.next()) {
				// get the data to map
				countryMap.put(rs.getString("country"), mapToObject(rs));
			}
			//db.close();
		} catch (SQLException ex) {
			Logger.getLogger(CountryRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return countryMap;
	}

	public void create(Country item) {
		create(Collections.singletonList(item));
	}

	public void create(List<Country> items) {
		// insert a new Country into the database
		HashMap<String, String> data = new HashMap<>();
		int insertId;

		for (Country item : items) {
			try {
				// to create a record, we need only the country name and the user who created it
				// TODO: use User object, instead of hardcoding "test"
				data.put("country", item.getCountry());
				data.put("createdBy", Auth.getLoggedInUser().getUserName());
				data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
				//db.open();
				insertId = db.insert("country", data);
				if (insertId > 0) {
					// create succeeded
					item.setId(insertId);
				}
			} catch (RuntimeException ex) {
				Logger.getLogger(CountryRepository.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				//db.close();
			}
		}
	}

	/**
	 * Save changes to an existing record in the database
	 *
	 * @param item
	 * @return boolean
	 */
	public boolean update(Country item) {
		boolean result = false;
		HashMap<String, String> data = new HashMap<>();
		
		try {
			// to create a record, we need only the country name and the user who created it
			// TODO: use User object, instead of hardcoding "test"
			data.put("countryId", item.getId().toString());
			data.put("country", item.getCountry());
			data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
			//db.open();
			result = db.update("country", data, "countryId");
			if(result == false) {
				// there was an error in updating
				Logger.getLogger(CountryRepository.class.getName()).log(Level.SEVERE, "MySql returned 0 updated rows!");
			}
		} catch (RuntimeException ex) {
			Logger.getLogger(CountryRepository.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			//db.close();
		}
		
		return result;
	}

	public boolean remove(Country item) {
		//db.open();
		
		boolean result = db.delete("country", "countryId", item.getId().toString());
		//db.close();
		
		return result;
	}

	public List<Country> query(String queryString) {
		return null;
	}

	/**
	 * 
	 * @param id
	 * @return Country
	 */
	public Country get(int id) {
		Country country = null;
		
		//db.open();

		ResultSet rs = null;
		rs = db.query("SELECT * FROM country WHERE countryId=" + Integer.toString(id));

		try {
			if (rs.next()) {
				// get the data to map
				country = mapToObject(rs);
			}
		} catch (SQLException ex) {
			Logger.getLogger(CountryRepository.class.getName()).log(Level.SEVERE, null, ex);
		}
			
		//db.close();		
		return country;
	}
}
