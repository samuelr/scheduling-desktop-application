/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuel
 */
public class CityRepository {

	private static City createCity(int id, String city, Country country, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String LastUpdateBy) {

		return new City(id, city, country, createDate, createdBy, lastUpdate, LastUpdateBy);
	}

	private static City mapToObject(ResultSet rs) throws SQLException {
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		
		return createCity(
				rs.getInt("cityId"),
				rs.getString("city"),
				countryRepo.get(rs.getInt("countryId")),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("lastUpdateBy")
		);
	}

	private Database db = null;

	public CityRepository(Database database) {
		this.db = database;
	}

	public HashMap<String, City> getAll() {
		HashMap<String, City> cityMap = new HashMap<>();

		//db.open();

		ResultSet rs = null;
		rs = db.query("SELECT * FROM city");

		try {
			while (rs.next()) {
				// get the data to map
				cityMap.put(rs.getString("city"), mapToObject(rs));
			}
			//db.close();
		} catch (SQLException ex) {
			Logger.getLogger(CityRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return cityMap;
	}

	public void create(City item) {
		create(Collections.singletonList(item));
	}

	public void create(List<City> items) {
		// insert a new City into the database
		HashMap<String, String> data = new HashMap<>();
		int insertId;

		for (City item : items) {
			try {
				// to create a record, we need the city name, countryId, and the user who created it
				// TODO: use User object, instead of hardcoding "test"
				data.put("city", item.getCity());
				data.put("countryId", item.getCountry().getId().toString());
				data.put("createdBy", Auth.getLoggedInUser().getUserName());
				data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
				//db.open();
				insertId = db.insert("city", data);
				if (insertId > 0) {
					// create succeeded
					item.setId(insertId);
				}
			} catch (RuntimeException ex) {
				Logger.getLogger(CityRepository.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				//db.close();
			}
		}
	}

	/**
	 * Save changes to an existing record in the database
	 *
	 * @param item
	 * @return boolean
	 */
	public boolean update(City item) {
		Boolean result = false;
		HashMap<String, String> data = new HashMap<>();

		try {
			// to update a record, we need the city name, countryId, and the user who created it
			// TODO: use User object, instead of hardcoding "test"
			data.put("cityId", Integer.toString(item.getId()));
			data.put("city", item.getCity());
			data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
			//db.open();
			result = db.update("city", data, "cityId");
			if(result == false) {
				// there was an error in updating
				Logger.getLogger(CityRepository.class.getName()).log(Level.SEVERE, "MySql returned 0 updated rows!");
			}
		} catch (RuntimeException ex) {
			Logger.getLogger(CityRepository.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			//db.close();
		}
		
		return result;
	}

	public boolean remove(City item) {
		//db.open();
		boolean result = db.delete("city", "cityId", Integer.toString(item.getId()));
		//db.close();
		
		return result;
	}

	public List<City> query(String queryString) {
		return null;
	}

	/**
	 * 
	 * @param id
	 * @return City
	 */
	public City get(int id) {
		City city = null;
		
		//db.open();

		ResultSet rs = null;
		rs = db.query("SELECT * FROM city WHERE cityId=" + Integer.toString(id));

		try {
			if (rs.next()) {
				// get the data to map
				city = mapToObject(rs);
			}
		} catch (SQLException ex) {
			Logger.getLogger(CityRepository.class.getName()).log(Level.SEVERE, null, ex);
		}
			
		//db.close();		
		return city;
	}
	
}
