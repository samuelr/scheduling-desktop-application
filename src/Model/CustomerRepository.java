/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Helper.Auth;
import Helper.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 *
 * @author Samuel
 */
public class CustomerRepository {

	private static Country createCountry(Integer id, String name, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		return new Country(id, name, createDate, createdBy, lastUpdate, lastUpdateBy);
	}

	private static City createCity(Integer id, String name, Country country, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		return new City(id, name, country, createDate, createdBy, lastUpdate, lastUpdateBy);
	}

	private static Address createAddress(Integer id, String address, String address2, City city, String postalCode, String phone, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		return new Address(id, address, address2, city, postalCode, phone, createDate, createdBy, lastUpdate, lastUpdateBy);
	}

	private static Customer createCustomer(Integer id, String name, Address address, Boolean active, ZonedDateTime createDate, String createdBy, ZonedDateTime lastUpdate, String lastUpdateBy) {
		return new Customer(id, name, address, active, createDate, createdBy, lastUpdate, lastUpdateBy);
	}

	private static Customer mapToObject(ResultSet rs) throws SQLException {
		// get the data to map
		Country country = createCountry(
				rs.getInt("country.countryId"),
				rs.getString("country.country"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("country.createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("country.createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("country.lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("country.LastUpdateBy")
		);

		City city = createCity(
				rs.getInt("city.cityId"),
				rs.getString("city.city"),
				country,
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("city.createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("city.createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("city.lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("city.LastUpdateBy")
		);

		Address address = createAddress(
				rs.getInt("address.addressId"),
				rs.getString("address.address"),
				rs.getString("address.address2"),
				city,
				rs.getString("address.postalcode"),
				rs.getString("address.phone"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("address.createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("address.createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("address.lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("address.LastUpdateBy")
		);

		return createCustomer(
				rs.getInt("customer.customerId"),
				rs.getString("customer.customerName"),
				address,
				rs.getBoolean("customer.active"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("address.createDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("address.createdBy"),
				ZonedDateTime.of(LocalDateTime.parse(rs.getString("address.lastUpdate"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")), ZoneId.of("GMT")),
				rs.getString("address.LastUpdateBy")
		);

	}

	private Database db = null;
	private HashMap<String, String> queries;
	private AddressRepository addressRepo;
	private CityRepository cityRepo;
	private CountryRepository countryRepo;

	public CustomerRepository(Database database) {
		this.db = database;
		queries = new HashMap<>();
		initQueries();
	}

	public CustomerRepository(Database db, AddressRepository addressRepo, CityRepository cityRepo, CountryRepository countryRepo) {
		this.db = db;
		this.addressRepo = addressRepo;
		this.cityRepo = cityRepo;
		this.countryRepo = countryRepo;
		queries = new HashMap<>();
		initQueries();
	}

	/**
	 * Creates a new customer record in the database
	 *
	 * @param name name
	 * @param active active
	 * @param address address
	 * @param address2 address2
	 * @param city city
	 * @param country country
	 * @param postalCode postalCode
	 * @param phone phone
	 * @return Customer | null
	 */
	public Customer create(String name, Boolean active, String address, String address2, String city, String country, String postalCode, String phone) {
		// create classes
		Country newCountry = new Country();
		newCountry.setCountry(country);

		City newCity = new City();
		newCity.setCity(city);
		newCity.setCountry(newCountry);

		Address newAddress = new Address();
		newAddress.setAddress(address);
		newAddress.setAddress2(address2);
		newAddress.setCity(newCity);
		newAddress.setPostalCode(postalCode);
		newAddress.setPhone(phone);

		Customer newCustomer = new Customer();
		newCustomer.setName(name);
		newCustomer.setActive(active);
		newCustomer.setAddress(newAddress);

		//db.open();
		// Start a transaction
		Connection conn = db.getConnection();
		try {
			conn.setAutoCommit(false);
		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		// try to insert the country first.
		HashMap<String, String> countryData = new HashMap<>();
		countryData.put("country", newCountry.getCountry());
		countryData.put("createdBy", "test");
		countryData.put("lastUpdateBy", "test");
		PreparedStatement addCountry = null;
		Integer newCountryId = null;
		try {
			addCountry = conn.prepareStatement(buildInsertQuery("country", countryData), Statement.RETURN_GENERATED_KEYS);
			addCountry.execute();
			ResultSet rs = addCountry.getGeneratedKeys();
			if (rs.next()) {
				// query succeeded
				newCountryId = rs.getInt(1);
				System.out.println("Add country succeeded. New Id: " + newCountryId);
			} else {
				// query failed
				System.out.println("Add country failed. Rolling back.");
				// rollback the transaction
				conn.rollback();
				return null;
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ex1) {
				Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex1);
			}
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		// try to insert the city
		HashMap<String, String> cityData = new HashMap<>();
		cityData.put("city", newCity.getCity());
		cityData.put("countryId", newCountryId.toString());
		cityData.put("createdBy", "test");
		cityData.put("lastUpdateBy", "test");
		Integer newCityId = null;
		PreparedStatement addCity = null;
		try {
			addCity = conn.prepareStatement(buildInsertQuery("city", cityData), Statement.RETURN_GENERATED_KEYS);
			addCity.execute();
			ResultSet rs = addCity.getGeneratedKeys();
			if (rs.next()) {
				// query succeeded
				newCityId = rs.getInt(1);
				System.out.println("Add city succeeded. New Id: " + newCityId);
			} else {
				// query failed
				// rollback the transaction
				conn.rollback();
				return null;
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ex1) {
				Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex1);
			}
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		// insert the address
		HashMap<String, String> addressData = new HashMap<>();
		addressData.put("address", newAddress.getAddress());
		addressData.put("address2", newAddress.getAddress2());
		addressData.put("cityId", newCityId.toString());
		addressData.put("postalCode", newAddress.getPostalCode());
		addressData.put("phone", newAddress.getPhone());
		addressData.put("createdBy", "test");
		addressData.put("lastUpdateBy", "test");
		Integer newAddressId = null;
		PreparedStatement addAddress = null;
		try {
			addAddress = conn.prepareStatement(buildInsertQuery("address", addressData), Statement.RETURN_GENERATED_KEYS);
			addAddress.execute();
			ResultSet rs = addAddress.getGeneratedKeys();
			if (rs.next()) {
				// query succeeded
				newAddressId = rs.getInt(1);
				System.out.println("Add address succeeded. New Id: " + newAddressId);
			} else {
				// query failed
				// rollback the transaction
				conn.rollback();
				return null;
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ex1) {
				Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex1);
			}
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		// insert the customer
		HashMap<String, String> customerData = new HashMap<>();
		customerData.put("customerName", newCustomer.getName());
		customerData.put("active", newCustomer.isActive() ? "1" : "0");
		customerData.put("addressId", newAddressId.toString());
		customerData.put("createdBy", Auth.getLoggedInUser().getUserName());
		customerData.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
		Integer newCustomerId = null;
		PreparedStatement addCustomer = null;
		try {
			addCustomer = conn.prepareStatement(buildInsertQuery("customer", customerData), Statement.RETURN_GENERATED_KEYS);
			addCustomer.execute();
			ResultSet rs = addCustomer.getGeneratedKeys();
			if (rs.next()) {
				// query succeeded
				newCustomerId = rs.getInt(1);
			} else {
				// query failed
				// rollback the transaction
				conn.rollback();
				return null;
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException ex1) {
				Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex1);
			}
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		// all queries succeeded
		try {
			conn.commit();
		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return get(newCustomerId);

	}

	/**
	 * Retrieves a Customer record from the database by Customer ID
	 *
	 * @param id id
	 * @return Customer | null
	 * @throws SQLException SQLException
	 */
	public Customer get(Integer id) {
		if (id < 1) {
			return null;
		}

		String query = queries.get("getAll").concat(" WHERE `customer.customerId` = " + id);

		//this.db.open();
		ResultSet rs = this.db.query(query);
		Customer newCust = null;
		try {
			if (rs.next()) {
				newCust = mapToObject(rs);

			}
		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class
					.getName()).log(Level.SEVERE, null, ex);
		}
		//this.db.close();
		return newCust;
	}

	public Customer getByName(String name) {
		String query = queries.get("getAll").concat(" WHERE `customer.customerName` LIKE '" + name + "'");

		ResultSet rs = this.db.query(query);
		Customer newCust = null;
		try {
			if (rs.next()) {
				newCust = mapToObject(rs);

			}
		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class
					.getName()).log(Level.SEVERE, null, ex);
		}
		return newCust;
	}
	
	/**
	 * Saves a customer record in the database.
	 *
	 * @param customer customer
	 * @return Customer
	 * @throws Exception exception
	 */
	public boolean udpate(Customer customer) {

		Connection connection = db.getConnection();

		System.out.println("Starting Customer update...");
		try {
			System.out.println("setAutoCommit(false)");
			connection.setAutoCommit(false);
			// look for modified objects
			// country
			if (customer.getAddress().getCity().getCountry().isModified()) {
				// save the country
				System.out.println("Attempting to save Country");
				if (countryRepo.update(customer.getAddress().getCity().getCountry()) == false) {
					System.out.println("Update of Country object failed. Aborting Customer update.");
					connection.rollback();
					return false;
				}
			}
			// city
			if (customer.getAddress().getCity().isModified()) {
				// save the city
				System.out.println("Attempting to save City");
				if (cityRepo.update(customer.getAddress().getCity()) == false) {
					System.out.println("Update of City object failed. Aborting Customer update.");
					connection.rollback();
					return false;
				}
			}

			// address 
			if (customer.getAddress().isModified()) {
				// save the address
				System.out.println("Attempting to save Address");
				if (addressRepo.update(customer.getAddress()) == false) {
					System.out.println("Update of Address object failed. Aborting Customer update.");
					connection.rollback();
					return false;
				}
			}

			// customer
			if (customer.isModified()) {
				// save the customer
				System.out.println("Attempting to save Customer");
				Boolean result;
				HashMap<String, String> data = new HashMap<>();

				try {
					// to update a record, we need the city name, countryId, and the user who created it
					// TODO: use User object, instead of hardcoding "test"
					data.put("customerId", Integer.toString(customer.getId()));
					data.put("customerName", customer.getName());
					data.put("active", (customer.isActive() ? "1" : "0"));
					data.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
					//db.open();
					result = db.update("customer", data, "customerId");

					if (result == false) {
						// there was an error in updating
						Logger.getLogger(AddressRepository.class
								.getName()).log(Level.SEVERE, "MySql returned 0 updated rows!");
					}
				} catch (RuntimeException ex) {
					System.out.println("Update of Customer object failed. Aborting Customer update.");
					connection.rollback();
					Logger
							.getLogger(AddressRepository.class
									.getName()).log(Level.SEVERE, null, ex);
				} finally {
					//db.close();
				}
			}

			// all updates succeeded. commit.
			System.out.println("All updates succeeded. Commit.");
			connection.commit();

			// reset setAutoCommit
			System.out.println("setAutoCommit(true)");
			connection.setAutoCommit(true);

		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class
					.getName()).log(Level.SEVERE, null, ex);
		}

		return true;
	}

	public ArrayList<Customer> getAllCustomers() throws SQLException {
		ArrayList<Customer> customers = new ArrayList();

		//db.open();
		ResultSet rs = null;
		rs = db.query(queries.get("getAll"));

		while (rs.next()) {
			// get the data to map
			customers.add(mapToObject(rs));
		}
		//db.close();
		return customers;
	}

	private void initQueries() {
		queries.put("getAll", "SELECT * FROM customers");
		queries.put("byID", " WHERE `customer.customerId` = ?");
		queries.put("updateCustomer", "UPDATE ");
	}

	private String buildInsertQuery(String table, HashMap<String, String> data) {
		// prepare the query
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO ").append(table).append(" SET ");

		// iterate the hashmap
		data.keySet().forEach((key) -> {
			query.append(key).append("='").append(data.get(key)).append("',");
		});
		// delete the last comma and add a semicolon
		query.delete(query.length() - 1, query.length()).append(";");
		return query.toString();
	}

	private String buildUpdateQuery(String table, String keyColumn, HashMap<String, String> data) {
		String id = data.get("#id");
		data.remove("#id");

		// prepare the query
		StringBuilder query = new StringBuilder();
		query.append("UPDATE ").append(table).append(" SET ");

		query.append(buildKeyValuePairs(data));
		query.append(" WHERE ").append(keyColumn).append("=").append(id);
		return query.toString();
	}

	private String buildKeyValuePairs(HashMap<String, String> data) {
		StringBuilder kvpString = new StringBuilder();
		// iterate the hashmap
		data.keySet().forEach((key) -> {
			kvpString.append(key).append("='").append(data.get(key)).append("',");
		});
		// delete the last comma
		kvpString.delete(kvpString.length() - 1, kvpString.length());
		return kvpString.toString();
	}

	public boolean remove(Customer customer) {
		Connection connection = db.getConnection();
		try {
			System.out.println("Starting remove Customer...");
			System.out.println("setAutoCommit(false)");
			connection.setAutoCommit(false);

			// delete Country
			System.out.println("Attempting to delete Country");
			if (countryRepo.remove(customer.getAddress().getCity().getCountry()) == false) {
				System.out.println("Removal of Country object failed. Aborting Customer removal.");
				connection.rollback();
				return false;
			}
			
			// delete customer
			System.out.println("Attempting to delete Customer");
			if(db.delete("customer", "customerId", Integer.toString(customer.getId())) == false) {
				System.out.println("Removal of Customer object failed. Aborting Customer removal.");
				connection.rollback();
				return false;
			}
			
			// delete Address
			System.out.println("Attempting to delete Address");
			if (addressRepo.remove(customer.getAddress()) == false) {
				System.out.println("Removal of Address object failed. Aborting Customer removal.");
				connection.rollback();
				return false;
			}
			
			// delete City
			System.out.println("Attempting to delete City");
			if (cityRepo.remove(customer.getAddress().getCity()) == false) {
				System.out.println("Removal of City object failed. Aborting Customer removal.");
				connection.rollback();
				return false;
			}
			
			
			
			// all deletes succeeded. commit.
			System.out.println("All deletes succeeded. Commit.");
			connection.commit();

			// reset setAutoCommit
			System.out.println("setAutoCommit(true)");
			connection.setAutoCommit(true);
			
		} catch (SQLException ex) {
			Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
		}

		return true;
	}
}
