/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.util.List;

/**
 * Read only Repository interface
 * 
 * @author Samuel
 */
public interface ReadOnlyRepository<T> {
	public List<T> query(Specification specification);
	public List<T> query(String queryString);
}
