/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

/**
 *
 * @author Samuel
 */
public interface SqlSpecification extends Specification {
    public String toSqlClause();
}