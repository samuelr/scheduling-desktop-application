/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.util.List;


/**
 * Interface for the Repository pattern
 * 
 * @author Samuel
 * @param <T>
 */
public interface Repository<T> extends ReadOnlyRepository<T> {
	public void create(T item);
	public void create(List<T> items);
	public void update(T item);
	public void remove(T item);
}
