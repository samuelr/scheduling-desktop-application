/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class CalendarController implements Initializable {

	//@FXML // fx:id="webCalendar"
	//private WebView webCalendar; // Value injected by FXMLLoader

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
		//WebEngine webEngine = webCalendar.getEngine();
		//webEngine.loadContent("<html><head></head><body><h1>Hello WebView!</h1><p>This is a web view in Java!</p></body></html>");
		//webEngine.load(getClass().getResource("/View_Controller/calendar.html").toExternalForm());
		generateCalendar();
	}

	private void generateCalendar() {
		// get a date object for now
	
		LocalDateTime now = LocalDateTime.now();
		
		// get the day of the week the month starts on
		System.out.println(now.getMonth().getValue());
	}
}
