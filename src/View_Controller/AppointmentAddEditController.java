/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.AppointmentTypes;
import Helper.Auth;
import Helper.Database;
import Helper.I18N;
import Helper.Locations;
import Helper.Schedule;
import Model.AddressRepository;
import Model.Appointment;
import Model.AppointmentRepository;
import Model.CityRepository;
import Model.CountryRepository;
import Model.Customer;
import Model.CustomerRepository;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class AppointmentAddEditController implements Initializable {

	private URL location;

	private ResourceBundle resources;

	@FXML // fx:id="ddLocation"
	private ComboBox<String> ddLocation; // Value injected by FXMLLoader

	@FXML // fx:id="txtContact"
	private TextField txtContact; // Value injected by FXMLLoader

	@FXML // fx:id="lblCustomer"
	private Label lblCustomer; // Value injected by FXMLLoader

	@FXML // fx:id="lblContact"
	private Label lblContact; // Value injected by FXMLLoader

	@FXML // fx:id="ddCustomer"
	private ComboBox<String> ddCustomer; // Value injected by FXMLLoader

	@FXML // fx:id="txtUrl"
	private TextField txtUrl; // Value injected by FXMLLoader

	@FXML // fx:id="txtAppTitle"
	private TextField txtAppointmentTitle; // Value injected by FXMLLoader

	@FXML // fx:id="txtDescription"
	private TextField txtDescription; // Value injected by FXMLLoader

	@FXML // fx:id="lblType"
	private Label lblType; // Value injected by FXMLLoader

	@FXML // fx:id="lblEnd"
	private Label lblEnd; // Value injected by FXMLLoader

	@FXML // fx:id="txtType"
	private ChoiceBox<String> ddType; // Value injected by FXMLLoader

	@FXML // fx:id="lblDate"
	private Label lblDate; // Value injected by FXMLLoader

	@FXML // fx:id="lblLocation"
	private Label lblLocation; // Value injected by FXMLLoader

	@FXML // fx:id="lblDescription"
	private Label lblDescription; // Value injected by FXMLLoader

	@FXML // fx:id="ddStartAMPM"
	private ChoiceBox<String> ddStartAMPM; // Value injected by FXMLLoader

	@FXML // fx:id="btnCancel"
	private Button btnCancel; // Value injected by FXMLLoader

	@FXML // fx:id="btnSave"
	private Button btnSave; // Value injected by FXMLLoader

	@FXML // fx:id="dpStartDate"
	private DatePicker dpDate; // Value injected by FXMLLoader

	@FXML // fx:id="txtEndTime"
	private TextField txtEndTime; // Value injected by FXMLLoader

	@FXML // fx:id="ddEndAMPM"
	private ChoiceBox<String> ddEndAMPM; // Value injected by FXMLLoader

	@FXML // fx:id="lblStart"
	private Label lblStart; // Value injected by FXMLLoader

	@FXML // fx:id="lblUrl"
	private Label lblUrl; // Value injected by FXMLLoader

	@FXML // fx:id="lblTitle"
	private Label lblTitle; // Value injected by FXMLLoader

	@FXML // fx:id="txtStartTime"
	private TextField txtStartTime; // Value injected by FXMLLoader

	@FXML // fx:id="txtTitle"
	private Label txtTitle; // Value injected by FXMLLoader

	@FXML
	private ComboBox<String> ddTimeZone;

	@FXML
	private Label lblTimeZone;

	private Appointment currentAppointment = null;

	@FXML
	void doSave(ActionEvent event) {
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		CustomerRepository customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);
		AppointmentRepository repo = new AppointmentRepository(Database.getInstance(), customerRepo);

		HashMap<String, Object> appointmentData = new HashMap<>();

		if (!validate()) {
			// invalid data
			return;
		}
		if (currentAppointment == null) {
			// we are adding a new customer
			// get the data from the fields and put it in a hashmap
			// validate the data

			Customer customer = customerRepo.getByName(ddCustomer.getSelectionModel().getSelectedItem());
			appointmentData.put("customerId", Integer.toString(customer.getId()));
			appointmentData.put("userId", Integer.toString(Auth.getLoggedInUser().getId()));
			appointmentData.put("title", txtAppointmentTitle.getText());
			appointmentData.put("description", txtDescription.getText());
			appointmentData.put("location", ddLocation.getSelectionModel().getSelectedItem());
			appointmentData.put("type", ddType.getSelectionModel().getSelectedItem());
			appointmentData.put("contact", txtContact.getText());
			appointmentData.put("url", txtUrl.getText());

			// put together the date and time
			int startHour = Integer.parseInt(txtStartTime.getText().trim().split(":")[0]);
			int startMinute = Integer.parseInt(txtStartTime.getText().split(":")[1]);
			int endHour = Integer.parseInt(txtEndTime.getText().trim().split(":")[0]);
			int endMinute = Integer.parseInt(txtEndTime.getText().split(":")[1]);

			LocalDate appointmentDate = dpDate.getValue();
			ZonedDateTime appointmentStart = ZonedDateTime.of(LocalDateTime.of(dpDate.getValue(), LocalTime.of(startHour, startMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem()));
			ZonedDateTime appointmentEnd = ZonedDateTime.of(LocalDateTime.of(dpDate.getValue(), LocalTime.of(endHour, endMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem()));
			if (startHour < 12 && ddStartAMPM.getValue().equals("PM")) {
				appointmentStart = appointmentStart.plusHours(12);
			}
			if (startHour == 12 && ddStartAMPM.getValue().equals("AM")) {
				appointmentStart = appointmentStart.minusHours(12);
			}
			if (endHour < 12 && ddEndAMPM.getValue().equals("PM")) {
				appointmentEnd = appointmentEnd.plusHours(12);
			}
			if (endHour == 12 && ddEndAMPM.getValue().equals("AM")) {
				appointmentEnd = appointmentEnd.minusHours(12);
			}
			appointmentData.put("start", appointmentStart.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("GMT"))));
			appointmentData.put("end", appointmentEnd.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("GMT"))));
			appointmentData.put("createdBy", Auth.getLoggedInUser().getUserName());
			appointmentData.put("lastUpdateBy", Auth.getLoggedInUser().getUserName());
			if (!repo.create(appointmentData)) {
				// failure
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Error saving appointment");
				Optional<ButtonType> result = alert.showAndWait();
				return;
			}
		} else {
			// we're saving an existing appointment
			Customer customer = customerRepo.getByName(ddCustomer.getSelectionModel().getSelectedItem());
			this.currentAppointment.setCustomer(customer);
			this.currentAppointment.setTitle(txtAppointmentTitle.getText());
			this.currentAppointment.setDescription(txtDescription.getText());
			this.currentAppointment.setLocation(ddLocation.getSelectionModel().getSelectedItem());
			this.currentAppointment.setType(ddType.getSelectionModel().getSelectedItem());
			this.currentAppointment.setContact(txtContact.getText());
			try {
				if (!txtUrl.getText().trim().isEmpty()) {
					this.currentAppointment.setUrl(new URL(txtUrl.getText().trim()));
				}
			} catch (MalformedURLException ex) {
				Logger.getLogger(AppointmentAddEditController.class.getName()).log(Level.INFO, "Error parsing URL field.", ex);
			}

			// put together the date and time
			int startHour = Integer.parseInt(txtStartTime.getText().trim().split(":")[0]);
			int startMinute = Integer.parseInt(txtStartTime.getText().split(":")[1]);
			int endHour = Integer.parseInt(txtEndTime.getText().trim().split(":")[0]);
			int endMinute = Integer.parseInt(txtEndTime.getText().split(":")[1]);

			LocalDate appointmentDate = dpDate.getValue();
			ZonedDateTime appointmentStart = ZonedDateTime.of(LocalDateTime.of(appointmentDate, LocalTime.of(startHour, startMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem()));
			ZonedDateTime appointmentEnd = ZonedDateTime.of(LocalDateTime.of(appointmentDate, LocalTime.of(endHour, endMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem()));
			if (startHour < 12 && ddStartAMPM.getValue().equals("PM")) {
				appointmentStart = appointmentStart.plusHours(12);
			}
			if (startHour == 12 && ddStartAMPM.getValue().equals("AM")) {
				appointmentStart = appointmentStart.minusHours(12);
			}
			if (endHour < 12 && ddEndAMPM.getValue().equals("PM")) {
				appointmentEnd = appointmentEnd.plusHours(12);
			}
			if (endHour == 12 && ddEndAMPM.getValue().equals("AM")) {
				appointmentEnd = appointmentEnd.minusHours(12);
			}
			this.currentAppointment.setStart(appointmentStart.withZoneSameInstant(ZoneId.of("GMT")));
			this.currentAppointment.setEnd(appointmentEnd.withZoneSameInstant(ZoneId.of("GMT")));

			if (!repo.update(this.currentAppointment)) {
				// failure
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Error saving appointment");
				Optional<ButtonType> result = alert.showAndWait();
				return;
			}
		}

		// close the window
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
	}

	@FXML
	void doCancel(ActionEvent event) throws IOException {

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(I18N.get("common.cancel"));
		alert.setHeaderText(null);
		alert.setContentText(I18N.get("common.msg.cancel"));
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			// don't update the info and go back to the main screen
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
		}
	}

	/**
	 * Initializes the controller class.
	 *
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.location = url;
		this.resources = rb;

		// fill types choicebox
		ddType.setItems(AppointmentTypes.toObservable());

		// fill locations combobox
		ddLocation.setItems(Locations.toObservable());
		ddLocation.getSelectionModel().select(0);

		Set<String> zones = ZoneId.getAvailableZoneIds();
		ddTimeZone.setItems(FXCollections.observableArrayList(zones).sorted());
		ddTimeZone.getSelectionModel().select(ZoneId.systemDefault().toString());

		// fill AM, PM dropdowns
		ArrayList<String> ampm = new ArrayList<>();
		ampm.add("AM");
		ampm.add("PM");
		ddStartAMPM.setItems(FXCollections.observableArrayList(ampm));
		ddStartAMPM.getSelectionModel().select(0);
		ddEndAMPM.setItems(FXCollections.observableArrayList(ampm));
		ddEndAMPM.getSelectionModel().select(0);

		// customer name drop down
		ObservableList<String> items = FXCollections.observableArrayList(customerNameList().values());
		ddCustomer.setItems(items);
//		ddCustomer.getEditor().textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				System.out.println(observable);
//				System.out.println(oldValue);
//				System.out.println(newValue);
//				TextField editor = ddCustomer.getEditor();
//				String selected = ddCustomer.getSelectionModel().getSelectedItem();
//
//				// Create a FilteredList wrapping the ObservableList.
//				FilteredList<String> filteredItems = new FilteredList<String>(items, p -> true);
//
//				// run this on the GUI thread
//				//Platform.runLater(() -> {
//				System.out.println("RunLater");
//
//				// If the no item in the list is selected or the selected item
//				// isn't equal to the current input, we refilter the list.
//				if (ddCustomer.getSelectionModel().getSelectedItem() == null
//						|| !ddCustomer.getSelectionModel().getSelectedItem().equals(newValue)) {
//					filteredItems.setPredicate(item -> {
//						// We return true for any items that starts with the
//						// same letters as the input. We use toUpperCase to
//						// avoid case sensitivity.
//						if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
//							return true;
//						} else {
//							return false;
//						}
//					});
//
//					// if there is an item selected and the newValue doesn't equal the 
//				}
//
//				//});
//				// set the items list and re-show the pop up to change the size of the pop up
//				ddCustomer.setItems(filteredItems);
//				ddCustomer.hide();
//				ddCustomer.setVisibleRowCount(filteredItems.size());
//				ddCustomer.show();
//			}
//		});
	}

	public void setTitle(String title) {
		txtTitle.setText(title);
	}

	void setAppointment(Appointment appointment) {
		this.currentAppointment = appointment;

		ddCustomer.getSelectionModel().select(appointment.getCustomer().getName());
		txtAppointmentTitle.setText(appointment.getTitle());
		txtDescription.setText(appointment.getDescription());
		ddType.getSelectionModel().select(appointment.getType());
		txtContact.setText(appointment.getContact());
		ddLocation.getSelectionModel().select(appointment.getLocation());
		txtUrl.setText((appointment.getUrl() == null) ? " " : appointment.getUrl().toString());
		dpDate.setValue(LocalDate.from(appointment.getStart().withZoneSameInstant(ZoneId.systemDefault())));
		int startHour = appointment.getStart().withZoneSameInstant(ZoneId.systemDefault()).getHour();
		int startMinute = appointment.getStart().withZoneSameInstant(ZoneId.systemDefault()).getMinute();
		if (startHour < 12) {
			ddStartAMPM.getSelectionModel().select("AM");
		} else {
			ddStartAMPM.getSelectionModel().select("PM");
			if (startHour > 12) {
				startHour = startHour - 12;
			}
		}
		txtStartTime.setText(String.format("%2d:%02d", startHour, startMinute).trim());

		int endHour = appointment.getEnd().withZoneSameInstant(ZoneId.systemDefault()).getHour();
		int endMinute = appointment.getEnd().withZoneSameInstant(ZoneId.systemDefault()).getMinute();
		if (endHour < 12) {
			ddEndAMPM.getSelectionModel().select("AM");
		} else {
			ddEndAMPM.getSelectionModel().select("PM");
			if (endHour > 12) {
				endHour = endHour - 12;
			}
		}
		txtEndTime.setText(String.format("%2d:%02d", endHour, endMinute).trim());

		ddTimeZone.getSelectionModel().select(appointment.getStart().withZoneSameInstant(ZoneId.systemDefault()).getZone().toString());
	}

	private ObservableMap<Integer, String> customerNameList() {
		ObservableMap<Integer, String> customers = FXCollections.observableHashMap();
		Database db = Database.getInstance();

		//db.open();
		ResultSet rs;
		rs = db.query("SELECT customerId, customerName FROM customer ORDER BY customerName ASC");

		try {
			while (rs.next()) {
				// get the data to map
				customers.put(rs.getInt("customerId"), rs.getString("customerName"));
			}
		} catch (SQLException ex) {
			Logger.getLogger(AppointmentAddEditController.class.getName()).log(Level.SEVERE, null, ex);
		}

		return customers;
	}

	/**
	 * Validates the form fields
	 *
	 * @return True if validation passes
	 */
	private boolean validate() {
		ArrayList<String> messages = new ArrayList();
		// Customer

		if (!ddCustomer.getItems().contains(ddCustomer.getSelectionModel().getSelectedItem())) {
			messages.add("Customer must be selected from the list.");
		}
		// date
		if (dpDate.getValue() == null) {
			messages.add("No date selected.");
		}
		// Start and end times
		// must be in the format "00:00"
		String timePattern = "(\\d{1,2}):(\\d{2})";
		String startTime = txtStartTime.getText().trim();
		String endTime = txtEndTime.getText().trim();
		if (!startTime.matches(timePattern) || !endTime.matches(timePattern)) {
			messages.add("Times must me in the format: HH:mm");
		}
		// make sure the time makes sense.
		int startHour = Integer.parseInt(startTime.split(":")[0]);
		int startMinute = Integer.parseInt(startTime.split(":")[1]);
		int endHour = Integer.parseInt(endTime.split(":")[0]);
		int endMinute = Integer.parseInt(endTime.split(":")[1]);
		if ((startHour < 1) || (startHour > 12) || (startMinute > 59)
				|| (endHour < 1) || (endHour > 12) || (endMinute > 59)) {
			messages.add("One or both of the times don't make sense.");
		} else {

			// apply am/pm
			if (startHour < 12 && ddStartAMPM.getSelectionModel().getSelectedItem().equals("PM")) {
				startHour += 12;
			}
			if (endHour < 12 && ddEndAMPM.getSelectionModel().getSelectedItem().equals("PM")) {
				endHour += 12;
			}
			// make sure the end time is after the start time
			if ((LocalTime.of(startHour, startMinute).isAfter(LocalTime.of(endHour, endMinute)))) {
				messages.add("End time must be after Start time.");
			}
			// make sure the appointment is within business hours
			if (!Schedule.isWithinHours(LocalTime.of(startHour, startMinute), LocalTime.of(endHour, endMinute))) {
				messages.add("Appointment must be within business hours");
			}

			// make sure the appointment time doesn't overlap another appointment
			if (Schedule.overlapsOther(
					ZonedDateTime.of(LocalDateTime.of(dpDate.getValue(), LocalTime.of(startHour, startMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem())),
					ZonedDateTime.of(LocalDateTime.of(dpDate.getValue(), LocalTime.of(endHour, endMinute)), ZoneId.of(ddTimeZone.getSelectionModel().getSelectedItem())),
					(currentAppointment != null) ? currentAppointment.getId() : 0
			)) {
				messages.add("This appointment time overlaps another appointment.");
			}
		}
		// are there messages?
		if (messages.size() > 0) {

			Alert alert = new Alert(Alert.AlertType.ERROR);

			alert.setTitle("Validation Errors");
			alert.setHeaderText(null);
			alert.setContentText("There were errors on the form");

			Label label = new Label("Please correct the following:");

			String messageText = "";
			for (String message : messages) {
				messageText += message + "\n";
			}
			TextArea textArea = new TextArea(messageText);

			textArea.setEditable(false);
			textArea.setWrapText(true);

			textArea.setMaxWidth(Double.MAX_VALUE);

			textArea.setMaxHeight(Double.MAX_VALUE);

			GridPane.setVgrow(textArea, Priority.ALWAYS);

			GridPane.setHgrow(textArea, Priority.ALWAYS);

			GridPane expContent = new GridPane();

			expContent.setMaxWidth(Double.MAX_VALUE);
			expContent.add(label, 0, 0);
			expContent.add(textArea, 0, 1);

			// Set expandable Exception into the dialog pane.
			alert.getDialogPane().setExpandableContent(expContent);
			alert.getDialogPane().setExpanded(true);

			alert.showAndWait();

			return false;
		}

		return true;
	}
}
