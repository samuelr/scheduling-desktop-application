/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.Database;
import Helper.I18N;
import Model.Address;
import Model.AddressRepository;
import Model.CityRepository;
import Model.CountryRepository;
import Model.Customer;
import Model.CustomerRepository;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class CustomerController implements Initializable {

	private ResourceBundle rb;

	@FXML // fx:id="tblCustomers"
	private TableView<Customer> tblCustomers; // Value injected by FXMLLoader

	@FXML // fx:id="colCustomerName"
	private TableColumn<Customer, String> colCustomerName; // Value injected by FXMLLoader

	@FXML // fx:id="colCustomerAddress"
	private TableColumn<Customer, Address> colCustomerAddress; // Value injected by FXMLLoader

	@FXML // fx:id="colCustomerPhone"
	private TableColumn<Customer, String> colCustomerPhone; // Value injected by FXMLLoader

	@FXML // fx:id="btnClose"
	private Button btnClose; // Value injected by FXMLLoader

	@FXML // fx:id="btnDelete"
	private Button btnDelete; // Value injected by FXMLLoader

	@FXML // fx:id="btnAdd"
	private Button btnAdd; // Value injected by FXMLLoader

	@FXML // fx:id="btnEdit"
	private Button btnEdit; // Value injected by FXMLLoader

	@FXML // fx:id="lblTitle"
	private Label lblTitle; // Value injected by FXMLLoader

	private CustomerRepository customerRepo;

	@FXML
	void doAdd(ActionEvent event) throws IOException {
		// open the window
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/CustomerAddEdit.fxml"));
		Stage stage = new Stage();
		Parent layout;

		layout = loader.load();
		Scene scene = new Scene(layout);
		stage.setScene(scene);
		CustomerAddEditController controller = loader.<CustomerAddEditController>getController();
		controller.setTitle(rb.getString("customer.add.title"));
		stage.setTitle(rb.getString("app.title"));
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);
		stage.showAndWait();

		loadCustomers();
	}

	@FXML
	void doEdit(ActionEvent event) throws IOException {
		Customer selectedCustomer = tblCustomers.getSelectionModel().getSelectedItem();
		if (selectedCustomer == null) {
			return;
		}
		// open the window
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/CustomerAddEdit.fxml"));
		Stage stage = new Stage();
		Parent layout;

		layout = loader.load();
		Scene scene = new Scene(layout);
		stage.setScene(scene);
		CustomerAddEditController controller = loader.<CustomerAddEditController>getController();
		controller.setTitle(rb.getString("customer.edit.title"));
		controller.setCustomer(selectedCustomer);
		stage.setTitle(rb.getString("app.title"));
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);
		stage.showAndWait();

		// reload the customers table
		loadCustomers();
	}

	@FXML
	void doDelete(ActionEvent event) {
		Customer selectedCustomer = tblCustomers.getSelectionModel().getSelectedItem();

		if (selectedCustomer == null) {
			return;
		}
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(I18N.get("customer.delete.title"));
		alert.setHeaderText(null);
		alert.setContentText(String.format(I18N.get("customer.delete.question"), selectedCustomer.getName()));
		Optional<ButtonType> result = alert.showAndWait();
		alert = null;
		if (result.get() == ButtonType.OK) {
			// remove customer
			//CustomerRepository repo = new CustomerRepository(Database.getInstance());
			if(this.customerRepo.remove(selectedCustomer) == false) {
				Alert removeAlert = new Alert(Alert.AlertType.INFORMATION);
				removeAlert.setTitle(I18N.get("message.error.removeError.title"));
				removeAlert.setHeaderText(null);
				removeAlert.setContentText(I18N.get("customer.error.removeError"));

				removeAlert.showAndWait();
			} else {
				loadCustomers();
			}
		}
	}

	@FXML
	void doClose(ActionEvent event) {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
	}

	/**
	 * Initializes the controller class.
	 *
	 * @param url Location of the code base
	 * @param rb Local strings
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.rb = rb;

		// create customer repo
		initCustomerRepo();

		// load text labels
		//tblCustomers.setPlaceholder(new Label(rb.getString("common.loading")));

		lblTitle.textProperty().bind(I18N.createStringBinding("customer.title"));
		colCustomerName.textProperty().bind(I18N.createStringBinding("customer.name"));
		colCustomerPhone.textProperty().bind(I18N.createStringBinding("customer.phone"));
		colCustomerAddress.textProperty().bind(I18N.createStringBinding("customer.address"));
		btnAdd.textProperty().bind(I18N.createStringBinding("customer.add"));
		btnEdit.textProperty().bind(I18N.createStringBinding("common.edit"));
		btnDelete.textProperty().bind(I18N.createStringBinding("customer.delete"));
		btnClose.textProperty().bind(I18N.createStringBinding("customer.close"));

		colCustomerName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		//colCustomerAddress.setCellValueFactory(cellData -> cellData.getValue().getAddress().fullAddressProperty());
		colCustomerPhone.setCellValueFactory(cellData -> cellData.getValue().getAddress().phoneProperty());
		colCustomerName.prefWidthProperty().bind(tblCustomers.widthProperty().multiply(0.333333));
		colCustomerPhone.prefWidthProperty().bind(tblCustomers.widthProperty().multiply(0.333333));
		colCustomerAddress.prefWidthProperty().bind(tblCustomers.widthProperty().multiply(0.333333));
//		colCustomerName.setCellFactory(column -> new TableCell<Customer, String>() {
//			private VBox graphic;
//			private Label valueLabel;
//
//			// Anonymous constructor:
//			{
//				graphic = new VBox();
//				valueLabel = createLabel();
//				graphic.getChildren().add(valueLabel);
//				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//			}
//
//			private Label createLabel() {
//				Label label = new Label();
//				VBox.setVgrow(label, Priority.ALWAYS);
//				label.setMaxWidth(Double.MAX_VALUE);
//				label.setMaxHeight(Double.MAX_VALUE);
//				//label.setStyle("-fx-background-color: #777777 ;");
//				label.setAlignment(Pos.CENTER_LEFT);
//				return label;
//			}
//
//			@Override
//			public void updateItem(String value, boolean empty) {
//				if (value == null) {
//					setGraphic(null);
//				} else {
//					valueLabel.setText(value);
//					setGraphic(graphic);
//				}
//			}
//		});
//		
//		colCustomerPhone.setCellFactory(column -> new TableCell<Customer, String>() {
//			private VBox graphic;
//			private Label valueLabel;
//
//			// Anonymous constructor:
//			{
//				graphic = new VBox();
//				valueLabel = createLabel();
//				graphic.getChildren().add(valueLabel);
//				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//			}
//
//			private Label createLabel() {
//				Label label = new Label();
//				VBox.setVgrow(label, Priority.ALWAYS);
//				label.setMaxWidth(Double.MAX_VALUE);
//				label.setMaxHeight(Double.MAX_VALUE);
//				//label.setStyle("-fx-background-color: #777777 ;");
//				label.setAlignment(Pos.CENTER_LEFT);
//				return label;
//			}
//
//			@Override
//			public void updateItem(String value, boolean empty) {
//				if (value == null) {
//					setGraphic(null);
//				} else {
//					valueLabel.setText(value);
//					setGraphic(graphic);
//				}
//			}
//		});

		colCustomerAddress.setCellValueFactory(cellData
				-> new ReadOnlyObjectWrapper<>(cellData.getValue().getAddress()));
		colCustomerAddress.setCellFactory(column -> new TableCell<Customer, Address>() {

			private VBox graphic;
			private Label address1Label;
			private Label address2Label;
			private Label cityPostCountryLabel;

			// Anonymous constructor:
			{
				graphic = new VBox();
				address1Label = createLabel();
				address2Label = createLabel();
				cityPostCountryLabel = createLabel();
				graphic.getChildren().addAll(address1Label,
						address2Label, cityPostCountryLabel);
				setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			}

			private final Label createLabel() {
				Label label = new Label();
				VBox.setVgrow(label, Priority.ALWAYS);
				label.setMaxWidth(Double.MAX_VALUE);
				label.setAlignment(Pos.CENTER_LEFT);
				return label;
			}

			@Override
			public void updateItem(Address address, boolean empty) {
				if (address == null) {
					setGraphic(null);
				} else {
					address1Label.setText(address.getAddress());
					address2Label.setText(address.getAddress2());

					StringBuilder cityPostCountryString = new StringBuilder();
					cityPostCountryString.append(address.getCity().getCity()).append(", ");
					cityPostCountryString.append(address.getPostalCode()).append(", ");
					cityPostCountryString.append(address.getCity().getCountry().getCountry());

					cityPostCountryLabel.setText(cityPostCountryString.toString());
					setGraphic(graphic);
				}
			}
		});
	}

	private void loadCustomers() {
		try {
			tblCustomers.setItems(FXCollections.observableArrayList(this.customerRepo.getAllCustomers()));
			// get the list of customers from the database and display in the table
		} catch (SQLException ex) {
			Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public EventHandler<WindowEvent> getOnShown() {
		return new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				Task getCustomers = new Task() {
					@Override
					protected Object call() throws Exception {
						loadCustomers();
						return null;
					}
				};
				Thread t = new Thread(getCustomers);
				t.start();
			}
		};
	}

	private void initCustomerRepo() {
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		this.customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);
	}
}
