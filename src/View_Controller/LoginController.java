/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.Auth;
import Helper.I18N;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class LoginController implements Initializable {

	private ResourceBundle rb;

	private ObjectProperty<Locale> locale;
	private HashMap<String, Locale> localeMap = new HashMap<>();

	@FXML // fx:id="lblUserName"
	private Label lblUserName; // Value injected by FXMLLoader

	@FXML // fx:id="ddLocation"
	private ComboBox<String> ddLocation; // Value injected by FXMLLoader

	@FXML // fx:id="ddLanguage"
	private ComboBox ddLanguage; // Value injected by FXMLLoader

	@FXML // fx:id="btnLogin"
	private Button btnLogin; // Value injected by FXMLLoader

	@FXML // fx:id="txtUserName"
	private TextField txtUserName; // Value injected by FXMLLoader

	@FXML // fx:id="lblPassword"
	private Label lblPassword; // Value injected by FXMLLoader

	@FXML // fx:id="lblTitle"
	private Label lblTitle; // Value injected by FXMLLoader

	@FXML // fx:id="txtPassword"
	private PasswordField txtPassword; // Value injected by FXMLLoader

	private boolean auth;
	private boolean cancel;

	@FXML
	void doLogin(ActionEvent event) {
		this.auth = Auth.login(txtUserName.getText(), txtPassword.getText());

//		UserRepository userRepo = new UserRepository(Database.getInstance());
//		User user = userRepo.getByName(txtUserName.getText());
//		if (user == null) {
//			Alert alert = new Alert(Alert.AlertType.ERROR);
//			alert.setTitle(I18N.get("login.error.title"));
//			alert.setHeaderText(null);
//			alert.setContentText(I18N.get("login.error.msg"));
//			Optional<ButtonType> result = alert.showAndWait();
//			return;
//		}
//
//		//check password
//		if (!user.getPassword().equals(Crypto.md5(txtPassword.getText()))) {
//			Alert alert = new Alert(Alert.AlertType.ERROR);
//			alert.setTitle(I18N.get("login.error.title"));
//			alert.setHeaderText(null);
//			alert.setContentText(I18N.get("login.error.msg"));
//			Optional<ButtonType> result = alert.showAndWait();
//			return;
//		}
		// authenticated. close window to continue
		//this.auth = true;
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
	}

	@FXML
	void doCancel(ActionEvent event) {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Exit program");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to exit?");
		Optional<ButtonType> result = alert.showAndWait();
		System.out.println(result.get().toString());
		if (result.get() == ButtonType.OK) {
			this.cancel = true;
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	void doHandleLanguage(ActionEvent event) {
		SingleSelectionModel model = ddLanguage.getSelectionModel();
		I18N.setLocale(localeMap.get(model.getSelectedItem().toString()));

		lblTitle.getScene().getWindow().sizeToScene();
	}

	/**
	 * Initializes the controller class.
	 *
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.rb = rb;
		this.auth = false;

		// debugging
		ddLanguage.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue ov, String t, String t1) {
				System.out.println(ov);
				System.out.println(t);
				System.out.println(t1);
			}
		});

		ObservableList<Locale> locales = FXCollections.observableArrayList(I18N.getSupportedLocales());
		locales.forEach(t -> {
			localeMap.putIfAbsent(t.getDisplayName(), t);
		});
		ddLanguage.setItems(FXCollections.observableArrayList(localeMap.keySet()));
		ddLanguage.getSelectionModel().selectFirst();

		// ToDo: iterate over loc.* strings instead of adding manually.
		ddLocation.getItems().addAll(
				I18N.get("loc.phoenix"),
				I18N.get("loc.new_york"),
				I18N.get("loc.london")
		);
		ddLocation.getSelectionModel().select(0);

		lblTitle.textProperty().bind(I18N.createStringBinding("login.title"));
		lblUserName.textProperty().bind(I18N.createStringBinding("common.username"));
		lblPassword.textProperty().bind(I18N.createStringBinding("common.password"));
		btnLogin.textProperty().bind(I18N.createStringBinding("common.login"));
		ddLocation.promptTextProperty().bind(I18N.createStringBinding("common.location"));

	}

	public EventHandler<WindowEvent> getOnShown() {
		// Lamba expression
		// The EventHandle class has one function: handle(T event)
		// Defining this one function this way is more efficient
		// than coding a return new EventHandler<WindowEvent>() {@Ooverride handle.....
		return (WindowEvent event) -> {
			// set focus to the username field
			txtUserName.requestFocus();
		};
	}

	public boolean isAuth() {
		return this.auth;
	}

	public boolean isCancel() {
		return this.cancel;
	}

}
