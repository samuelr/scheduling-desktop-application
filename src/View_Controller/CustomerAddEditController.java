/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.Database;
import Helper.I18N;
import Model.AddressRepository;
import Model.CityRepository;
import Model.Country;
import Model.CountryRepository;
import Model.Customer;
import Model.CustomerRepository;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class CustomerAddEditController implements Initializable {

	private Customer currentCustomer;

	@FXML // fx:id="txtTitle"
	private Label txtTitle; // Value injected by FXMLLoader

	@FXML // fx:id="lblName"
	private Label lblName; // Value injected by FXMLLoader

	@FXML // fx:id="lblCity"
	private Label lblCity; // Value injected by FXMLLoader

	@FXML // fx:id="lblAddress1"
	private Label lblAddress1; // Value injected by FXMLLoader

	@FXML // fx:id="lblAddress2"
	private Label lblAddress2; // Value injected by FXMLLoader

	@FXML // fx:id="lblCountry"
	private Label lblCountry; // Value injected by FXMLLoader

	@FXML // fx:id="lblPostalCode"
	private Label lblPostalCode; // Value injected by FXMLLoader

	@FXML // fx:id="lblPhone"
	private Label lblPhone; // Value injected by FXMLLoader

	@FXML // fx:id="txtName"
	private TextField txtName; // Value injected by FXMLLoader

	@FXML // fx:id="txtAddress1"
	private TextField txtAddress1; // Value injected by FXMLLoader

	@FXML // fx:id="txtAddress2"
	private TextField txtAddress2; // Value injected by FXMLLoader

	@FXML // fx:id="txtCity"
	private TextField txtCity; // Value injected by FXMLLoader

	@FXML // fx:id="txtCountry"
	private TextField txtCountry; // Value injected by FXMLLoader

	@FXML // fx:id="txtPostalCode"
	private TextField txtPostalCode; // Value injected by FXMLLoader

	@FXML // fx:id="txtPhone"
	private TextField txtPhone; // Value injected by FXMLLoader

	@FXML // fx:id="btnCancel"
	private Button btnCancel; // Value injected by FXMLLoader

	@FXML // fx:id="btnSave"
	private Button btnSave; // Value injected by FXMLLoader
	
	@FXML
	private CheckBox cbActive;
	
	@FXML
	private Label lblActive;
	
	private ResourceBundle rb;
	private CustomerRepository customerRepo;
	

	@FXML
	void doSave(ActionEvent event) {
		if (currentCustomer == null) {
			// We are adding a new customer
			CustomerRepository repo = new CustomerRepository(Database.getInstance());
			currentCustomer = repo.create(
					txtName.getText(),
					Boolean.TRUE,
					txtAddress1.getText(),
					txtAddress2.getText(),
					txtCity.getText(),
					txtCountry.getText(),
					txtPostalCode.getText(),
					txtPhone.getText()
			);
		} else {
			// validate and update the data
			currentCustomer.setName(txtName.getText());
			currentCustomer.setActive(cbActive.isSelected());
			currentCustomer.getAddress().setAddress(txtAddress1.getText());
			currentCustomer.getAddress().setAddress2(txtAddress2.getText());
			currentCustomer.getAddress().setPostalCode(txtPostalCode.getText());
			currentCustomer.getAddress().getCity().setCity(txtCity.getText());
			currentCustomer.getAddress().getCity().getCountry().setCountry(txtCountry.getText());
			currentCustomer.getAddress().setPhone(txtPhone.getText());

			try {
				customerRepo.udpate(currentCustomer);
			} catch (Exception ex) {
				Logger.getLogger(CustomerAddEditController.class.getName()).log(Level.SEVERE, null, ex);
				// show an error message
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle(I18N.get("message.error.saveError"));
				alert.setHeaderText(null);
				alert.setContentText(ex.getMessage());

				alert.showAndWait();
				return;
			}
		}
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		stage.close();
	}

	@FXML
	void doCancel(ActionEvent event) throws IOException {

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(I18N.get("common.cancel"));
		alert.setHeaderText(null);
		alert.setContentText(I18N.get("common.msg.cancel"));
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			// don't update the info and go back to the main screen
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
		}
	}

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.rb = rb;

		currentCustomer = null;
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		this.customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);
		
		cbActive.setSelected(true);
		cbActive.setIndeterminate(false);
		
		//txtTitle.textProperty().bind(I18N.createStringBinding("customer.title"));
		lblName.textProperty().bind(I18N.createStringBinding("customer.name"));
		lblActive.textProperty().bind(I18N.createStringBinding("customer.active"));
		lblAddress1.textProperty().bind(I18N.createStringBinding("customer.address"));
		lblAddress2.textProperty().bind(I18N.createStringBinding("customer.address2"));
		lblCity.textProperty().bind(I18N.createStringBinding("customer.city"));
		lblPostalCode.textProperty().bind(I18N.createStringBinding("customer.postalCode"));
		lblCountry.textProperty().bind(I18N.createStringBinding("customer.country"));
		lblPhone.textProperty().bind(I18N.createStringBinding("customer.phone"));

		//CountryRepository countryRepo = new CountryRepository(Database.getDatabase());
		HashMap<String, Country> countryMap = countryRepo.getAll();
		countryMap.forEach((index, item) -> System.out.println(index + "" + item.getCountry()));

		
	}

	public void setCustomer(Customer customer) {
		this.currentCustomer = customer;

		txtName.setText(customer.getName());
		cbActive.setSelected(customer.isActive());
		txtAddress1.setText(customer.getAddress().getAddress());
		txtAddress2.setText(customer.getAddress().getAddress2());
		txtCity.setText(customer.getAddress().getCity().getCity());
		txtPostalCode.setText(customer.getAddress().getPostalCode());
		txtCountry.setText(customer.getAddress().getCity().getCountry().getCountry());
		txtPhone.setText(customer.getAddress().getPhone());

//		// bind the data into the fields
//		txtName.textProperty().bindBidirectional(customer.nameProperty());
//		txtAddress1.textProperty().bind(customer.getAddress().addressProperty());
//		txtAddress2.textProperty().bind(customer.getAddress().address2Property());
//		txtCity.textProperty().bind(customer.getAddress().getCity().cityProperty());
//		txtPostalCode.textProperty().bind(customer.getAddress().postalCodeProperty());
//		txtCountry.textProperty().bind(customer.getAddress().getCity().getCountry().countryProperty());
//		txtPhone.textProperty().bind(customer.getAddress().getPhoneProperty());
	}

	public void setTitle(String title) {
		txtTitle.setText(title);
	}
}
