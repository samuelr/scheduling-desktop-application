/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.Database;
import Helper.HTML;
import Helper.ReportTypes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class ReportController implements Initializable {

	private URL url;

	private ResourceBundle resources;

	@FXML
	private WebView webReport;

	@FXML
	private ChoiceBox<String> ddReportType;

	@FXML
	private Label lblReportType;

	@FXML
	private Button btnRunReport;

	@FXML
	void doRunReport(ActionEvent event) {
		String query;
		// which report?
		if (ddReportType.getSelectionModel().getSelectedItem().equals(ReportTypes.APPOINTMENT_TYPES)) {
			query = "SELECT \n"
					+ "    type,\n"
					+ "    COUNT(type) AS count,\n"
					+ "    MONTH(start) AS month,\n"
					+ "    YEAR(start) AS year,\n"
					+ "    appointment.start AS datetime\n"
					+ "FROM\n"
					+ "    appointment\n"
					+ "GROUP BY month, type\n"
					+ "ORDER BY datetime , type;";
			ResultSet rs = Database.getInstance().query(query);
			// make array
			HashMap<Integer, ArrayList<HashMap<String, String>>> reportData = new HashMap<>();
			try {
				while (rs.next()) {
					if (!reportData.containsKey(rs.getInt("month"))) {
						reportData.put(rs.getInt("month"), new ArrayList<>());
					}
					HashMap<String, String> info = new HashMap<>();
					info.put("Type", rs.getString("type"));
					info.put("Count", rs.getString("count"));
					reportData.get(rs.getInt("month")).add(info);
				}
			} catch (SQLException ex) {
				Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
			}

			// load contents of report file
			String fileContents = readFile("/templates/AppointmentTypesReport.html");
			String finalOutput = fileContents.replace("{reportContents}", HTML.getAppointmentTypesMarkup(reportData));
			webReport.getEngine().loadContent(finalOutput);
			//webReport.getEngine().loadContent("Report under construction.");
		} else if (ddReportType.getSelectionModel().getSelectedItem().equals(ReportTypes.CONSULTANT_SCHEDULES)) {
			query = "SELECT \n"
					+ "    user.userName,\n"
					+ "    customer.customerName,\n"
					+ "    appointment.title,\n"
					+ "    appointment.description,\n"
					+ "    appointment.location,\n"
					+ "    appointment.contact,\n"
					+ "    appointment.url,\n"
					+ "    appointment.type,\n"
					+ "    appointment.start,\n"
					+ "    appointment.end\n"
					+ "FROM\n"
					+ "    user,\n"
					+ "    customer,\n"
					+ "    appointment\n"
					+ "WHERE\n"
					+ "    user.userId = appointment.userId\n"
					+ "        AND customer.customerId = appointment.customerId\n"
					+ "ORDER BY appointment.userId, appointment.start;";
			ResultSet rs = Database.getInstance().query(query);
			// make array
			HashMap<String, ArrayList<HashMap<String, String>>> schedule = new HashMap<>();
			try {
				while (rs.next()) {
					if (!schedule.containsKey(rs.getString("userName"))) {
						schedule.put(rs.getString("userName"), new ArrayList<>());
					}
					HashMap<String, String> info = new HashMap<>();
					info.put("customerName", rs.getString("customerName"));
					info.put("title", rs.getString("title"));
					info.put("description", rs.getString("description"));
					info.put("location", rs.getString("location"));
					info.put("contact", rs.getString("contact"));
					info.put("url", rs.getString("url"));
					info.put("type", rs.getString("type"));
					info.put("start", rs.getString("start"));
					info.put("end", rs.getString("end"));
					schedule.get(rs.getString("userName")).add(info);
				}
			} catch (SQLException ex) {
				Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
			}

			// load contents of report file
			String fileContents = readFile("/templates/ScheduleReport.html");
			String finalOutput = fileContents.replace("{userSchedule}", HTML.getShcheduleMarkup(schedule));
			webReport.getEngine().loadContent(finalOutput);
		} else if (ddReportType.getSelectionModel().getSelectedItem().equals(ReportTypes.BY_CUSTOMER)) {
			query = "SELECT \n"
					+ "    appointment.*,\n"
					+ "    user.userName as userName,\n"
					+ "    customer.customerName as customerName\n"
					+ "FROM \n"
					+ "    appointment, user, customer\n"
					+ "WHERE \n"
					+ "    user.userId = appointment.userid\n"
					+ "AND \n"
					+ "    customer.customerId = appointment.customerId\n"
					+ "ORDER BY appointment.customerId , start";
			ResultSet rs = Database.getInstance().query(query);
			// make array
			HashMap<String, ArrayList<HashMap<String, String>>> reportData = new HashMap<>();
			try {
				while (rs.next()) {
					if (!reportData.containsKey(rs.getString("customerName"))) {
						reportData.put(rs.getString("customerName"), new ArrayList<>());
					}
					HashMap<String, String> info = new HashMap<>();
					info.put("userName", rs.getString("userName"));
					info.put("title", rs.getString("title"));
					info.put("description", rs.getString("description"));
					info.put("location", rs.getString("location"));
					info.put("contact", rs.getString("contact"));
					info.put("url", rs.getString("url"));
					info.put("type", rs.getString("type"));
					info.put("start", rs.getString("start"));
					info.put("end", rs.getString("end"));
					reportData.get(rs.getString("customerName")).add(info);
				}
			} catch (SQLException ex) {
				Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
			}

			// load contents of report file
			String fileContents = readFile("/templates/ByCustomerReport.html");
			String finalOutput = fileContents.replace("{reportContents}", HTML.getByCustomerMarkup(reportData));
			webReport.getEngine().loadContent(finalOutput);
			//webReport.getEngine().getDocument().load(getClass().getResource("/templates/CustomerReport.html").toString());
			//webReport.getEngine().loadContent("Report under construction.");
		}
	}

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb
	) {
		this.url = url;
		this.resources = rb;

		ddReportType.setItems(FXCollections.observableArrayList(Arrays.asList(ReportTypes.APPOINTMENT_TYPES, ReportTypes.CONSULTANT_SCHEDULES, ReportTypes.BY_CUSTOMER)));
		ddReportType.getSelectionModel().selectFirst();

	}

	private String readFile(String file) {
		InputStream contentFile = getClass().getResourceAsStream(file);
		BufferedReader br;
		StringBuilder fileContents = new StringBuilder();
		try {
			br = new BufferedReader(new InputStreamReader(contentFile));
			String line;
			line = br.readLine();
			while (line != null) {
				fileContents.append(line);
				line = br.readLine();
			}
			br.close();
		} catch (IOException ex) {
			Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
		}
		return fileContents.toString();
	}
}
