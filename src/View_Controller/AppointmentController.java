/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View_Controller;

import Helper.Database;
import Helper.I18N;
import Model.AddressRepository;
import Model.Appointment;
import Model.AppointmentRepository;
import Model.CityRepository;
import Model.CountryRepository;
import Model.Customer;
import Model.CustomerRepository;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Samuel
 */
public class AppointmentController implements Initializable {

	private ResourceBundle resources;

	private URL location;

	@FXML
	private RadioButton rbWeek;

	@FXML
	private RadioButton rbMonth;

	@FXML
	private ToggleGroup viewBy;

	@FXML // fx:id="colDate"
	private TableColumn<Appointment, String> colDate; // Value injected by FXMLLoader

	@FXML // fx:id="colCustomerName"
	private TableColumn<Appointment, Hyperlink> colCustomerName; // Value injected by FXMLLoader

	@FXML // fx:id="btnAdd"
	private Button btnAdd; // Value injected by FXMLLoader

	@FXML // fx:id="btnEdit"
	private Button btnEdit; // Value injected by FXMLLoader

	@FXML // fx:id="colType"
	private TableColumn<Appointment, String> colType; // Value injected by FXMLLoader

	@FXML // fx:id="colTitle"
	private TableColumn<Appointment, String> colTitle; // Value injected by FXMLLoader

	@FXML // fx:id="tblAppointments"
	private TableView<Appointment> tblAppointments; // Value injected by FXMLLoader

	@FXML // fx:id="colStart"
	private TableColumn<Appointment, String> colStart; // Value injected by FXMLLoader

	@FXML // fx:id="colEnd"
	private TableColumn<Appointment, String> colEnd; // Value injected by FXMLLoader

	@FXML // fx:id="lblTitle"
	private Label lblTitle; // Value injected by FXMLLoader

	@FXML // fx:id="btnDelete"
	private Button btnDelete; // Value injected by FXMLLoader

	@FXML // fx:id="btnClose"
	private Button btnClose; // Value injected by FXMLLoader

	@FXML
	private Button btnPrev;

	@FXML
	private Button btnNext;

	@FXML
	private Label lblWeekMonth;
	
	@FXML
	private Button btnCustomerEditor;
	
	@FXML
	private Button btnReports;

	private AppointmentRepository appointmentRepo;
	private CustomerRepository customerRepo;

	private List<Appointment> appointmentsList;

	private ZonedDateTime tableFilterStart;

	@FXML
	void doAdd(ActionEvent event) throws IOException {
		// open the window
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/AppointmentAddEdit.fxml"));
		Stage stage = new Stage();
		Parent layout;

		layout = loader.load();
		Scene scene = new Scene(layout);
		stage.setScene(scene);
		AppointmentAddEditController controller = loader.<AppointmentAddEditController>getController();
		controller.setTitle(resources.getString("appointments.add.title"));
		stage.setTitle(resources.getString("app.title"));
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);
		stage.showAndWait();

		loadAppointments();
		setItemsFilter();
	}

	@FXML
	void doEdit(ActionEvent event) throws IOException {
		Appointment selectedAppointment = tblAppointments.getSelectionModel().getSelectedItem();
		if (selectedAppointment == null) {
			return;
		}
		// open the window
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/AppointmentAddEdit.fxml"));
		Stage stage = new Stage();
		Parent layout;

		layout = loader.load();
		Scene scene = new Scene(layout);
		stage.setScene(scene);
		AppointmentAddEditController controller = loader.<AppointmentAddEditController>getController();
		controller.setTitle(resources.getString("appointments.edit.title"));
		controller.setAppointment(selectedAppointment);
		stage.setTitle(resources.getString("app.title"));
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);
		stage.showAndWait();

		// reload the customers table
		loadAppointments();
		setItemsFilter();
	}

	@FXML
	void doDelete(ActionEvent event) {
		Appointment selectedAppointment = tblAppointments.getSelectionModel().getSelectedItem();

		if (selectedAppointment == null) {
			return;
		}
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(I18N.get("appointments.delete.question"));
		alert.setHeaderText(null);
		alert.setContentText("Delete customer: \"" + selectedAppointment.getTitle() + "\" with " + selectedAppointment.getCustomer().getName() + "?");
		Optional<ButtonType> result = alert.showAndWait();
		alert = null;
		if (result.get() == ButtonType.OK) {
			// remove customer
			//CustomerRepository repo = new CustomerRepository(Database.getInstance());
			if (this.appointmentRepo.remove(selectedAppointment) == false) {
				Alert removeAlert = new Alert(Alert.AlertType.INFORMATION);
				removeAlert.setTitle("Remove Error");
				removeAlert.setHeaderText(null);
				removeAlert.setContentText("Error removing customer");

				removeAlert.showAndWait();
			} else {
				loadAppointments();
				setItemsFilter();
			}
		}
	}

	@FXML
	void doPrev(ActionEvent event) {
		if (rbWeek.isSelected()) {
			// go back a week
			tableFilterStart = tableFilterStart.minusWeeks(1);
		} else {
			// go back a month
			tableFilterStart = tableFilterStart.minusMonths(1).withDayOfMonth(1);
		}
		System.out.println("New tableFilterStart: " + tableFilterStart.format(DateTimeFormatter.ofPattern("MM-dd-YYYY")));
		setWeekMonthText();
		setItemsFilter();
	}

	@FXML
	void doNext(ActionEvent event) {
//		if (currentWeek < 54) {
//			currentWeek = currentWeek + 1;
//			if (currentWeek == 54) {
//				currentWeek = 53;
//			}
//			setWeekMonthText();
//			setItemsFilter();
//		}
		if (rbWeek.isSelected()) {
			tableFilterStart = tableFilterStart.plusWeeks(1);
		} else {
			tableFilterStart = tableFilterStart.plusMonths(1).withDayOfMonth(1);
		}
		System.out.println("New tableFilterStart: " + tableFilterStart.format(DateTimeFormatter.ofPattern("MM-dd-YYYY HH:mm:ss")));
		setWeekMonthText();
		setItemsFilter();
	}

	@FXML
	void doClose(ActionEvent event) {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Exit program");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to exit the program?");
		Optional<ButtonType> result = alert.showAndWait();
		System.out.println(result.get().toString());
		if (result.get() == ButtonType.OK) {
			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	void doUpdateSpinner(ActionEvent event) {
		if (rbWeek.isSelected()) {
			// reset the tableFilterStart to the first day of the week it is in
			WeekFields weekfields = WeekFields.of(I18N.getLocale());
			tableFilterStart = tableFilterStart.with(ChronoField.ALIGNED_WEEK_OF_YEAR, tableFilterStart.minusWeeks(1).get(weekfields.weekOfWeekBasedYear())).with(weekfields.getFirstDayOfWeek()).withHour(0).withMinute(0).withSecond(0).withNano(0);
		} else {
			tableFilterStart = tableFilterStart.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
		}
		System.out.println("New tableFilterStart: " + tableFilterStart.format(DateTimeFormatter.ofPattern("MM-dd-YYYY HH:mm:ss")));
		setWeekMonthText();
		setItemsFilter();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.location = location;
		this.resources = resources;

		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		this.customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);
		this.appointmentRepo = new AppointmentRepository(Database.getInstance(), customerRepo);

		// by default, choose to show appointments by week
		rbWeek.setSelected(true);

		// tell the columns how to handle the Appointment object
		colCustomerName.setCellFactory((TableColumn<Appointment, Hyperlink> param) -> {
			TableCell<Appointment, Hyperlink> cell = new TableCell<Appointment, Hyperlink>() {
				@Override
				protected void updateItem(Hyperlink item, boolean empty) {
					setGraphic(item);
				}
			};
			return cell;
		});

		colCustomerName.setCellValueFactory((TableColumn.CellDataFeatures<Appointment, Hyperlink> cellData) -> {
			Hyperlink link = new Hyperlink(cellData.getValue().getCustomer().getName());
			link.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					// open the customer add/edit window.
					Customer selectedCustomer = cellData.getValue().getCustomer();
					if (selectedCustomer == null) {
						return;
					}
					// open the window
					FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/CustomerAddEdit.fxml"));
					Stage stage = new Stage();
					Parent layout = null;

					try {
						layout = loader.load();
					} catch (IOException ex) {
						Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
					}
					Scene scene = new Scene(layout);
					stage.setScene(scene);
					CustomerAddEditController controller = loader.<CustomerAddEditController>getController();
					controller.setTitle(I18N.get("customer.edit.title"));
					controller.setCustomer(selectedCustomer);
					stage.setTitle(I18N.get("app.title"));
					stage.initOwner(((Node) event.getSource()).getScene().getWindow());
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setResizable(false);
					stage.showAndWait();
				}
			});
			return new SimpleObjectProperty<>(link);
		});
		colType.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
		colStart.setCellValueFactory(cellData -> cellData.getValue().startProperty());
		colEnd.setCellValueFactory(cellData -> cellData.getValue().startProperty());
		colTitle.setCellValueFactory(cellData -> cellData.getValue().userProperty());

		colCustomerName.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.20));
		colType.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.20));
		colTitle.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.25));
		colDate.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.12));
		colStart.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.12));
		colEnd.prefWidthProperty().bind(tblAppointments.widthProperty().multiply(0.11));

		//tblAppointments.setPlaceholder(new Label(resources.getString("common.loading")));
		tblAppointments.setPlaceholder(new Label(""));

		// bind strings for locale
		lblTitle.textProperty().bind(I18N.createStringBinding("appointments.title"));
		colCustomerName.textProperty().bind(I18N.createStringBinding("customer.title"));
		colType.textProperty().bind(I18N.createStringBinding("appointments.type"));
		colStart.textProperty().bind(I18N.createStringBinding("appointments.start"));
		colEnd.textProperty().bind(I18N.createStringBinding("appointments.end"));
		colTitle.textProperty().bind(I18N.createStringBinding("appointments.appointmentTitle"));
		btnAdd.textProperty().bind(I18N.createStringBinding("common.add"));
		btnEdit.textProperty().bind(I18N.createStringBinding("common.modify"));
		btnDelete.textProperty().bind(I18N.createStringBinding("common.delete"));
		btnClose.textProperty().bind(I18N.createStringBinding("common.close"));

		// cell factories
		// view the start and end times in the time zone
		// of the currently logged in user.
		colStart.setCellValueFactory((cellData) -> {
			//cellData.getValue().getStart().format(DateTimeFormatter.ofPattern("hh:mm a"));
			return new ReadOnlyObjectWrapper<>(cellData.getValue().getStart().withZoneSameInstant(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("hh:mm a")));
		});
		colEnd.setCellValueFactory((cellData) -> {
			return new ReadOnlyObjectWrapper<>(cellData.getValue().getEnd().withZoneSameInstant(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("hh:mm a")));
		});
		colTitle.setCellValueFactory((cellData) -> {
			return new ReadOnlyObjectWrapper<>(cellData.getValue().getTitle());
		});
		colDate.setCellValueFactory((cellData) -> {
			return new ReadOnlyObjectWrapper<>(cellData.getValue().getStart().withZoneSameInstant(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("MM-dd-YYYY")));
		});

		// Initialize tableFilterStart
		WeekFields weekfields = WeekFields.of(I18N.getLocale());
		ZonedDateTime today = ZonedDateTime.now();
		if (rbWeek.isSelected()) {
			// set tableFilterStart to the first day of the currently selected week
			tableFilterStart = today.with(ChronoField.ALIGNED_WEEK_OF_YEAR, today.minusWeeks(1).get(weekfields.weekOfWeekBasedYear())).with(weekfields.getFirstDayOfWeek()).withHour(0).withMinute(0).withSecond(0).withNano(0);
		} else {
			// set tableFilterStart to the first day of the currently selected month
			tableFilterStart = today.withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0).withNano(0);
		}

		// set the label text
		setWeekMonthText();
	}

	private void setWeekMonthText() {
		if (rbWeek.isSelected()) {
			// set the label with Week of
			lblWeekMonth.setText("Week of " + tableFilterStart.format(DateTimeFormatter.ofPattern("dd MMM YYYY")));
		} else {
			// set the label with Month of
			lblWeekMonth.setText("Month of " + tableFilterStart.format(DateTimeFormatter.ofPattern("MMMM YYYY")));
		}

	}

	public void loadAppointments() {
		//List<Appointment> appointmentList = this.appointmentRepo.getAll();

		List<Appointment> appointments = this.appointmentRepo.getAll();
		//
		//tblAppointments.setItems(FXCollections.observableArrayList(appointmentList));

		// instead of setting the items from  here, put them in a class variable
		this.appointmentsList = appointments;
	}

	public void setItemsFilter() {
		ArrayList<Appointment> filteredList = new ArrayList<>();
		for (Appointment app : this.appointmentsList) {
			if (rbWeek.isSelected()) {
				if (isBetween(app.getStart(), tableFilterStart.minusDays(1), tableFilterStart.plusWeeks(1))) {
					filteredList.add(app);
				}
			} else {
				if (isBetween(app.getStart(), tableFilterStart, tableFilterStart.plusMonths(1))) {
					filteredList.add(app);
				}
			}
		}
		tblAppointments.setItems(FXCollections.observableArrayList(filteredList));
	}

	public EventHandler<WindowEvent> getOnShown() {
		// Lamba expression
		// The EventHandle class has one function: handle(T event)
		// Defining this one function this way is more efficient
		// than coding a return new EventHandler<WindowEvent>() {@Ooverride handle.....
		return (WindowEvent event) -> {
			Task getAppointmentsTask = new Task() {
				@Override
				protected Object call() throws Exception {
					loadAppointments();
					setItemsFilter();
					return null;
				}
			};

			tblAppointments.setPlaceholder(new Label("Loading..."));

			Thread t = new Thread(getAppointmentsTask);
			t.start();
			try {
				t.join();
			} catch (InterruptedException ex) {
				Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
			}

			System.out.println("Loading done.");

			tblAppointments.setPlaceholder(new Label("No items to display"));
		};
	}

	private boolean isBetween(ZonedDateTime test, ZonedDateTime start, ZonedDateTime end) {
		return (test.isAfter(start)) && (test.isBefore(end));
	}
	
	@FXML
	void doCustomerEditor(ActionEvent event) {
		FXMLLoader customerLoader = new FXMLLoader(getClass().getResource("/View_Controller/Customer.fxml"), resources);
		Parent customerParent = null;
		try {
			customerParent = customerLoader.load();
		} catch (IOException ex) {
			Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
		}
		CustomerController customerController = customerLoader.getController();
		Stage stage = new Stage(StageStyle.UNIFIED);
		stage.setResizable(false);
		stage.setScene(new Scene(customerParent));
		stage.setOnShown(customerController.getOnShown());
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.showAndWait();
	}
	
	@FXML
	void doReports(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/View_Controller/Report.fxml"), resources);
		Parent root = null;
		try {
			root = loader.load();
		} catch (IOException ex) {
			Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
		}
		ReportController controller = loader.getController();
		Stage stage = new Stage(StageStyle.UNIFIED);
		stage.setResizable(true);
		stage.setScene(new Scene(root));
		stage.initOwner(((Node) event.getSource()).getScene().getWindow());
		//stage.initModality(Modality.WINDOW_MODAL);
		stage.showAndWait();
	}
}
