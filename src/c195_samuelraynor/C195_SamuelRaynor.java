/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195_samuelraynor;

import Helper.Alerts;
import Helper.Auth;
import Helper.Database;
import Helper.I18N;
import Helper.Schedule;
import View_Controller.AppointmentController;
import View_Controller.CustomerController;
import View_Controller.LoginController;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Samuel
 */
public class C195_SamuelRaynor extends Application {

	@Override
	public void stop() throws Exception {
		// close the database connection
		Database.getInstance().close();
	}

	@Override
	public void start(Stage mainStage) throws Exception {
		
		// get strings
		Locale currentLocale = Locale.getDefault();
		ResourceBundle StringsBundle = ResourceBundle.getBundle("View_Controller/Strings", currentLocale);

		// connect to the database
		//Alert alert = new Alert(Alert.AlertType.NONE);
//		alert.setTitle("Connecting...");
//		alert.setHeaderText(null);
//		alert.setContentText("Connecting to database");
//		//alert.initModality(Modality.APPLICATION_MODAL);
//		alert.initStyle(StageStyle.UTILITY);
		//alert.show();
		Database.getInstance().open();
		//alert.hide();

		// Login first
		FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("/View_Controller/Login.fxml"), StringsBundle);
		Stage loginStage = new Stage();
		Parent loginParent = loginLoader.load();

		Scene loginScene = new Scene(loginParent);
		LoginController loginController = loginLoader.getController();
		loginStage.setOnShown(loginController.getOnShown());
		loginStage.setTitle(I18N.get("app.title"));
		loginStage.setResizable(false);
		loginStage.setScene(loginScene);

		while (!loginController.isAuth()) {
			loginStage.showAndWait();
			if (loginController.isCancel()) {
				Platform.exit();
				// return to break out of the function.
				// Platform.exit() is asyncronous and returns immediatly.
				// Without this, the program would create a new window and not exit.
				return;
			}
		}
		
		// Now we're logged in
		
		// check to see if this user has any appointments in the next 15 minutes
		if(Schedule.checkPendingAppointments(Auth.getLoggedInUser())) {
			// notify the user that there is an appointment
			Alerts.info("You have an appointment within the next 15 minutes. Look at the calendar.", "Pending Appointment");
		}
		
		// show the main screen
		// create window
		FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/View_Controller/Appointment.fxml"), StringsBundle);
		Parent mainParent = mainLoader.load();
		AppointmentController appointmentController = mainLoader.getController();

//			FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/View_Controller/Customer.fxml"), StringsBundle);
//			Parent mainParent = mainLoader.load();
//			CustomerController customerController = mainLoader.getController();
		Scene mainScene = new Scene(mainParent);

		mainStage.setTitle(StringsBundle.getString("app.title"));
		mainStage.setResizable(false);
		mainStage.setScene(mainScene);
		mainStage.setOnShown(appointmentController.getOnShown());
//			mainStage.setOnShown(customerController.getOnShown());
		mainStage.show();

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		launch(args);

	}
}
