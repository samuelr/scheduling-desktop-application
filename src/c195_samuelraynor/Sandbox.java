/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package c195_samuelraynor;

import Helper.Crypto;
import Helper.Database;
import Helper.I18N;
import Model.AddressRepository;
import Model.AppointmentRepository;
import Model.CityRepository;
import Model.CountryRepository;
import Model.CustomerRepository;
import Model.User;
import Model.UserRepository;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 *
 * @author Samuel
 */
public class Sandbox extends Application {

	private static String buildKeyValuePairs(HashMap<String, String> data) {
		StringBuilder kvpString = new StringBuilder();
		// iterate the hashmap
		data.keySet().forEach((key) -> {
			kvpString.append(key).append("='").append(data.get(key)).append("',");
		});
		// delete the last comma
		kvpString.delete(kvpString.length() - 1, kvpString.length());
		return kvpString.toString();
	}

	@Override
	public void start(Stage stage) throws Exception {

		// open database connection
		Database.getInstance().open();

		int currentWeek = 25;
		//ZonedDateTime today = ZonedDateTime.now();
		WeekFields weekfields = WeekFields.of(I18N.getLocale());
		ZonedDateTime today = ZonedDateTime.now().with(ChronoField.ALIGNED_WEEK_OF_YEAR, currentWeek).with(weekfields.getFirstDayOfWeek());
		System.out.println(today);
		
		int weekNumber = today.get(weekfields.weekOfWeekBasedYear());
		System.out.println(weekNumber);
		ZonedDateTime firstDayOfWeek = today.minusWeeks(1);
		System.out.println(today.minusWeeks(1).with(weekfields.getFirstDayOfWeek()).minusDays(1));
		System.out.println(firstDayOfWeek.with(weekfields.getFirstDayOfWeek()).format(DateTimeFormatter.ofPattern("dd MMM YYYY")));
		ZonedDateTime afterLastDayofWeek = today.with(ChronoField.ALIGNED_WEEK_OF_YEAR, currentWeek).with(weekfields.getFirstDayOfWeek()).plusDays(7);
		System.out.println(afterLastDayofWeek);
		System.out.println(isBetween(today, today.minusWeeks(1).with(weekfields.getFirstDayOfWeek()).minusDays(1), today.with(weekfields.getFirstDayOfWeek())) ? "true" : "false");
		
		
		Platform.exit();

		if (true) {
			return;
		}

		String str = "0:25";

		boolean matches = str.matches("(\\d{1,2}):(\\d{2})");
		System.out.println(matches);

		if (matches) {
			String[] matches2 = str.split(":");
			System.out.println(String.format("%02d", Integer.parseInt(matches2[0])));
			System.out.println(String.format("%02d", Integer.parseInt(matches2[1])));

		}
		Platform.exit();

		if (true) {
			return;
		}
//		System.out.println(Crypto.md5("test"));
//		System.out.println(Crypto.md5("password"));
//		System.out.println(Crypto.md5("r4nD0m13$"));
//		System.out.println(Crypto.md5("test"));
//		HashMap<String, String> data = new HashMap<>();
//		data.put("countryId", "1");
//		data.put("country", "US");
//		data.put("createdBy", "test");
//		data.put("lastUpdateBy", "test");
//		System.out.println(data.toString().substring(1,data.toString().length()-1));
//		System.out.println("UPDATE `" + "country" + "` SET(" + buildKeyValuePairs(data)
//				+ ") WHERE " + "countryId" + "=" + data.get("countryId"));

//		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
//
//		// create a country
//		Country newCountry = new Country();
//		newCountry.setCountry("MyCountry1");
//		countryRepo.create(newCountry);
//		System.out.println(newCountry.getId());
//
//		// update a country
//		Country country = countryRepo.get(newCountry.getId());
//		if (country == null) {
//			System.out.println("Country is null");
//		} else {
//			System.out.println(country.getCountry());
//			country.setCountry(country.getCountry() + "x");
//			countryRepo.update(country);
//			Country country2 = countryRepo.get(newCountry.getId());
//			System.out.println(country2.getCountry());
//		}
//
//		// create a city
//		CityRepository cityRepo = new CityRepository(Database.getInstance());
//		City newCity = new City();
//		newCity.setCity("Phoenix, AZ");
//		newCity.setCountry(country);
//		cityRepo.create(newCity);
//		System.out.println(newCity.getId());
//
//		// update a city
//		// update the city name
//		City city = cityRepo.get(newCity.getId());
//		if (city == null) {
//			System.out.println("City is null");
//		} else {
//			System.out.println(city.getCity());
//			city.setCity(city.getCity() + "x");
//			cityRepo.update(city);
//			City city2 = cityRepo.get(newCity.getId());
//			System.out.println(city2.getCity());
//		}
//
//		// create an address
//		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
//		Address newAddress = new Address();
//		newAddress.setAddress("123 My Street");
//		newAddress.setAddress2("APT 2");
//		newAddress.setPostalCode("99203");
//		newAddress.setPhone("5095551212");
//		newAddress.setCity(city);
//		addressRepo.create(newAddress);
//		System.out.println(newAddress.getId());
//
//		// update an address
//		Address address = addressRepo.get(newAddress.getId());
//		if (address == null) {
//			System.out.println("Address is null");
//		} else {
//			System.out.println(address.getFullAddress());
//			address.setAddress(address.getAddress() + "x");
//			addressRepo.update(address);
//			Address address2 = addressRepo.get(newAddress.getId());
//			System.out.println(address2.getAddress());
//		}
//
//		// remove an address
//		if (addressRepo.remove(address) == false) {
//			System.out.println("Removal of Address " + Integer.toString(address.getId()) + " failed.");
//		} else {
//			System.out.println("Removal of Address " + Integer.toString(address.getId()) + " successful.");
//		}
//
//		// remove a city
//		if (cityRepo.remove(city) == false) {
//			System.out.println("Removal of City " + Integer.toString(city.getId()) + " failed.");
//		} else {
//			System.out.println("Removal of City " + Integer.toString(city.getId()) + " successful.");
//		}
//
//		// remove a country
//		if (countryRepo.remove(country) == false) {
//			System.out.println("Removal of Country " + country.getId().toString() + " failed.");
//		} else {
//			System.out.println("Removal of Country " + country.getId().toString() + " successful.");
//		}
		CountryRepository countryRepo = new CountryRepository(Database.getInstance());
		CityRepository cityRepo = new CityRepository(Database.getInstance());
		AddressRepository addressRepo = new AddressRepository(Database.getInstance());
		CustomerRepository customerRepo = new CustomerRepository(Database.getInstance(), addressRepo, cityRepo, countryRepo);

		//Customer newCustomer = customerRepo.create("Samuel Raynor", true, "123 My Street C", "APT 2", "CityName", "USA", "99203", "5095551212");
		//customerRepo.remove(newCustomer);
		AppointmentRepository appRepo = new AppointmentRepository(Database.getInstance(), customerRepo);

//		List<Appointment> appList = appRepo.getAll();
//		System.out.println(Arrays.deepToString(appList.toArray()));
		UserRepository userRepo = new UserRepository(Database.getInstance());
		User user = new User();
		user.setUserName("test4");
		user.setPassword(Crypto.md5("password"));
		user.setActive(true);

		userRepo.create(user);

		User user2 = userRepo.getById(user.getId());
		System.out.println(user2.getUserName());

		//userRepo.remove(user2);
		Platform.exit();

		// get strings
//		Locale currentLocale = Locale.getDefault();
//		ResourceBundle StringsBundle = ResourceBundle.getBundle("View_Controller/Strings", currentLocale);
		// create window
//		Parent root = FXMLLoader.load(getClass().getResource("/View_Controller/Login.fxml"), StringsBundle);
//		
//		Scene scene = new Scene(root);
//		
//		
//		stage.setTitle(StringsBundle.getString("app.title"));
//		stage.setResizable(false);
//		stage.setScene(scene);
////		stage.show();
//		Country country = CountryFactory.getCountry(
//				0,
//				"CountryName",
//				null,
//				"test",
//				null,
//				"test"
//		);
//
//		CountryRepository repo = new CountryRepository(Database.getDatabase());
//		repo.add(country);
//
//		Platform.exit();
	}

	@Override
	public void stop() throws Exception {
		Database.getInstance().close();
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	private boolean isBetween(ZonedDateTime test, ZonedDateTime start, ZonedDateTime end) {
		return (test.isAfter(start)) && (test.isBefore(end));
	}
}
