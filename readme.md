# Scheduling Desktop Application

This project is something I created for my Software Development degree.
It is an appointment scheduling application supporting multiple users.

This program utilizes the following skills:

* Exception handling
* Date and Time functions
* Internationalization
* Using a database to retrieve and store information
* Using JavaFX 8 for user interface
* Using Tasks and Threads to accomplish database functions.
* MySQL database features such as Views, and Triggers


